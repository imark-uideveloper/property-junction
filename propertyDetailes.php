<?php include 'include/header.php'; ?>
<div class="pageContainer innerPage propertyDetails">
	<section class="banner innerBanner" style="background-image: url(images/innerBanner.jpg);">
		<div class="bannerContainer">
			<div class="bannerWrapper">
				<div class="container">
					<div class="bannerInner">
						<h1>Properties</h1>
						<p><a href="index">Home</a><span>/</span><span>Properties</span></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd">
		<div class="container">
			<div class="PropertyHead">
				<div class="PropertyHeadInner">
					<h3>!! AMAZING !! 2 Bedroom Apartment with SEA VIEW Available For Rent In Ocean Heights - Dubai Marina</h3>
					<div class="latestPorpConWrapper">
						<span>
							<svg class="pin" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 188.171 305.961">
								<g>
									<g>
										<path d="M188.024,93.942L188.024,93.942C188.024,42.067,145.957,0,94.082,0S0.15,42.067,0.15,93.942 c0,0-8.055,99.292,93.932,212.019C195.887,193.438,188.045,94.307,188.024,93.942z M94.082,160.326 c-35.279,0-63.881-28.612-63.881-63.871c0-35.299,28.602-63.871,63.881-63.871c35.279,0,63.871,28.572,63.871,63.871 C157.953,131.714,129.361,160.326,94.082,160.326z"></path>
										<path d="M94.082,49.038c24.965,0,45.188,20.223,45.188,45.168c0,24.944-20.223,45.188-45.188,45.188 c-24.955,0-45.178-20.243-45.178-45.188C48.904,69.261,69.127,49.038,94.082,49.038z"></path>
									</g>
								</g>
							</svg>
							<small>Northbrook, IL, United States</small>
						</span>
					</div>
				</div>
				<div class="PropertyHeadInner">
					<span>
						<span>AED 420,000</span>
						<span>AED 777/sqm</span>
					</span>
				</div>
			</div>

			<div class="wrapper">
				<div id="product__slider">
					<div class="product__slider-main">
						<div class="slide">
							<div class="slideImage">
								<img src="images/propertyDetailes.jpg" alt="Property Detailes">
							</div>
							<!-- <figure style="background-image: url(images/propertyDetailes.jpg);"></figure> -->
						</div>
						<div class="slide">
							<div class="slideImage">
								<img src="images/propertyDetailes.jpg" alt="Property Detailes">
							</div>
							<!-- <figure style="background-image: url(images/propertyDetailes.jpg);"></figure> -->
						</div>
						<div class="slide">
							<div class="slideImage">
								<img src="images/propertyDetailes.jpg" alt="Property Detailes">
							</div>
							<!-- <figure style="background-image: url(images/propertyDetailes.jpg);"></figure> -->
						</div>
						<div class="slide">
							<div class="slideImage">
								<img src="images/propertyDetailes.jpg" alt="Property Detailes">
							</div>
							<!-- <figure style="background-image: url(images/propertyDetailes.jpg);"></figure> -->
						</div>
						<div class="slide">
							<div class="slideImage">
								<img src="images/propertyDetailes.jpg" alt="Property Detailes">
							</div>
							<!-- <figure style="background-image: url(images/propertyDetailes.jpg);"></figure> -->
						</div>
						<div class="slide">
							<div class="slideImage">
								<img src="images/propertyDetailes.jpg" alt="Property Detailes">
							</div>
							<!-- <figure style="background-image: url(images/propertyDetailes.jpg);"></figure> -->
						</div>
						<div class="slide">
							<div class="slideImage">
								<img src="images/propertyDetailes.jpg" alt="Property Detailes">
							</div>
							<!-- <figure style="background-image: url(images/propertyDetailes.jpg);"></figure> -->
						</div>
					</div>

					<div class="product__slider-thmb">
						<div class="slide">
							<div class="slideImage">
								<img src="images/propertyDetailes1.jpg" alt="Property Detailes 1">
								<!-- <figure style="background-image: url(images/propertyDetailes1.jpg);"></figure> -->
							</div>
						</div>
						<div class="slide">
							<div class="slideImage">
								<img src="images/propertyDetailes2.jpg" alt="Property Detailes 2">
								<!-- <figure style="background-image: url(images/propertyDetailes2.jpg);"></figure> -->
							</div>
						</div>
						<div class="slide">
							<div class="slideImage">
								<img src="images/propertyDetailes3.jpg" alt="Property Detailes 3">
								<!-- <figure style="background-image: url(images/propertyDetailes3.jpg);"></figure> -->
							</div>
						</div>
						<div class="slide">
							<div class="slideImage">
								<img src="images/propertyDetailes4.jpg" alt="Property Detailes 4">
								<!-- <figure style="background-image: url(images/propertyDetailes4.jpg);"></figure> -->
							</div>
						</div>
						<div class="slide">
							<div class="slideImage">
								<img src="images/propertyDetailes5.jpg" alt="Property Detailes 5">
								<!-- <figure style="background-image: url(images/propertyDetailes5.jpg);"></figure> -->
							</div>
						</div>
						<div class="slide">
							<div class="slideImage">
								<img src="images/propertyDetailes6.jpg" alt="Property Detailes 6">
								<!-- <figure style="background-image: url(images/propertyDetailes6.jpg);"></figure> -->
							</div>
						</div>
						<div class="slide">
							<div class="slideImage">
								<img src="images/propertyDetailes7.jpg" alt="Property Detailes 7">
								<!-- <figure style="background-image: url(images/propertyDetailes7.jpg);"></figure> -->
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row propertyContant">
				<div class="col-sm-9 propertyContantInner">
					<div class="proDetails bgGray">
						<div class="proDetailsInner">
							<strong>570</strong>
							<span>sq m</span>
						</div>
						<div class="proDetailsInner">
							<strong>3</strong>
							<span>Rooms</span>
						</div>
						<div class="proDetailsInner">
							<strong>1</strong>
							<span>Badrooms</span>
						</div>
						<div class="proDetailsInner">
							<strong>2</strong>
							<span>Bathrooms</span>
						</div>
						<div class="proDetailsInner">
							<strong>Type</strong>
							<span>Apartment</span>
						</div>
						<div class="proDetailsInner">
							<strong>Contract</strong>
							<span>Rent</span>
						</div>
						<div class="proDetailsInner">
							<strong>Area</strong>
							<span>4275.00</span>
						</div>
					</div>

					<h5>Description</h5>
					<p>Ut euismod ultricies sollicitudin. Curabitur sed dapibus nulla. Nulla eget iaculis lectus. Mauris ac maximus neque. Nam in mauris quis libero sodales eleifend. Morbi varius, nulla sit amet rutrum elementum, est elit finibus tellus, ut tristique elit risus at metus. Sed fermentum, lorem vitae efficitur imperdiet, neque velit tristique turpis, et iaculis mi tortor finibus turpis.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat, velit risus accumsan nisl, eget tempor lacus est vel nunc. Proin accumsan elit sed neque euismod fringilla. Curabitur lobortis nunc velit, et fermentum urna dapibus non. Vivamus magna lorem, elementum id gravida ac, laoreet tristique augue. Maecenas dictum lacus eu nunc porttitor, ut hendrerit arcu efficitur.</p>

					<p>Nam mattis lobortis felis eu blandit. Morbi tellus ligula, interdum sit amet ipsum et, viverra hendrerit lectus. Nunc efficitur sem vel est laoreet, sed bibendum eros viverra. Vestibulum finibus, ligula sed euismod tincidunt, lacus libero lobortis ligula, sit amet molestie ipsum purus ut tortor. Nunc varius, dui et sollicitudin facilisis, erat felis imperdiet felis, et iaculis dui magna vitae diam. Donec mattis diam nisl, quis ullamcorper enim malesuada non. Curabitur lobortis eu mauris nec vestibulum. Nam efficitur, ex ac semper malesuada, nisi odio consequat dui, hendrerit vulputate odio dui vitae massa. Aliquam tortor urna, tincidunt ut euismod quis, semper vel ipsum. Ut non vestibulum mauris. Morbi euismod, felis non hendrerit viverra, nunc sapien bibendum ligula, eget vehicula nunc dolor eu ex. Quisque in semper odio. Donec auctor blandit ligula. Integer id lectus non nibh vulputate efficitur quis at arcu.</p>

					<h5>Details</h5>
					<div class="DetailsList">
						<ul>
							<li><strong>Building Age: </strong> <span>2 Years</span></li>
							<li><strong>Cooling: </strong> <span>Central Cooling</span></li>
							<li><strong>Sewer: </strong> <span>Public/City</span></li>
							<li><strong>Sewer: </strong> <span>Public/City</span></li>
							<li><strong>Parking: </strong> <span>Attached Garage</span></li>
							<li><strong>Heating: </strong> <span>Forced Air, Gas</span></li>
							<li><strong>Water: </strong> <span>City</span></li>
							<li><strong>Water: </strong> <span>City</span></li>
						</ul>
					</div>

					<h5>Features</h5>
					<div class="DetailsList">
						<ul>
							<li>
								<input type="checkbox" id="alarm" name="alarm">
								<label class="checkboxLabel" for="alarm">
									<span style="background-image: url(images/checkboxImage.png);"></span>
								</label>
								<span>Alarm</span>
							</li>
							<li>
								<input type="checkbox" id="internet" name="internet">
								<label class="checkboxLabel" for="internet">
									<span style="background-image: url(images/checkboxImage.png);"></span>
								</label>
								<span>Internet</span>
							</li>
							<li>
								<input type="checkbox" id="windowcovering" name="windowcovering">
								<label class="checkboxLabel" for="windowcovering">
									<span style="background-image: url(images/checkboxImage.png);"></span>
								</label>
								<span>Window Covering</span>
							</li>
							<li>
								<input type="checkbox" id="windowcovering1" name="windowcovering1">
								<label class="checkboxLabel" for="windowcovering1">
									<span style="background-image: url(images/checkboxImage.png);"></span>
								</label>
								<span>Window Covering</span>
							</li>
							<li>
								<input type="checkbox" id="gym" name="gym">
								<label class="checkboxLabel" for="gym">
									<span style="background-image: url(images/checkboxImage.png);"></span>
								</label>
								<span>Gym</span>
							</li>
							<li>
								<input type="checkbox" id="SwimmingPool" name="SwimmingPool">
								<label class="checkboxLabel" for="SwimmingPool">
									<span style="background-image: url(images/checkboxImage.png);"></span>
								</label>
								<span>Swimming Pool</span>
							</li>
							<li>
								<input type="checkbox" id="gym1" name="gym1">
								<label class="checkboxLabel" for="gym1">
									<span style="background-image: url(images/checkboxImage.png);"></span>
								</label>
								<span>Gym</span>
							</li>
							<li>
								<input type="checkbox" id="gym2" name="gym2">
								<label class="checkboxLabel" for="gym2">
									<span style="background-image: url(images/checkboxImage.png);"></span>
								</label>
								<span>Gym</span>
							</li>
						</ul>
					</div>

					<h5>Location</h5>

					<div id="map" class="PropertyMapWrapper"></div>
				</div>
				<div class="col-sm-3 sidebarContainer">
					<div class="formContainer PropertyContactForm">
						<form>
							<div class="form-group">
								<input type="text" class="form-control firstCap" placeholder="Name">
							</div>
							<div class="form-group">
								<input type="email" class="form-control" placeholder="Email">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Phone">
							</div>
							<div class="form-group">
								<textarea class="form-control firstCap" placeholder="Message"></textarea>
							</div>
							<div class="form-group">
								<input type="submit" name="message" value="Send">
							</div>
						</form>
					</div>

					<div class="sideInner">
						<h4>Featured Properties</h4>

						<div class="owl-carousel owl-theme">
							<div class="item">
								<figure style="background-image: url('images/proImg7.jpg')">
									<span>ADE 1,900/sq ft</span>
								</figure>
							</div>
							<div class="item">
								<figure style="background-image: url('images/proImg7.jpg')">
									<span>ADE 1,900/sq ft</span>
								</figure>
							</div>
							<div class="item">
								<figure style="background-image: url('images/proImg7.jpg')">
									<span>ADE 1,900/sq ft</span>
								</figure>
							</div>
							<div class="item">
								<figure style="background-image: url('images/proImg7.jpg')">
									<span>ADE 1,900/sq ft</span>
								</figure>
							</div>
							<div class="item">
								<figure style="background-image: url('images/proImg7.jpg')">
									<span>ADE 1,900/sq ft</span>
								</figure>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCFpaIaYgSLqlwlhHp_4R-QksOKZV9UugQ&callback=initMap"></script>
<script>
	/************************************************ Google Map ************************************************/
	function initMap() {
		var lock = {lat: 25.1132428, lng: 55.2028571};
		var map = new google.maps.Map(document.getElementById('map'), {
			disableDefaultUI: true,
			center: lock,
			zoom: 14
		});

		// Create markers.
		var marker = new google.maps.Marker({
			icon: 'images/marker.png',
			map: map,
			position: lock
		});
	}
	/************************************************ Google Map ************************************************/
</script>
<?php include 'include/footer.php'; ?>