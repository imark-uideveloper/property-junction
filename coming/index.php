<?php include 'inc/header.php'; ?>
<div class="coming-soon coming-soon1" style="background-image: url('images/main-bg.png');">
	<div class="coming-inner">
		<div class="logo"><img src="images/logo.png" alt="Logo"></div>
		<div class="boxCon">
			<h2><span>our new website</span></h2>
			<h1>website is under construction for better user experience</h1>
		</div>
		<div class="footer">
			<h3>
				For all inquiries: amish@propertyjunction.ae<br>
				Property Junction Real Estate 909 Dusseldorf Business Point<br>
				Al Barsha 1, P.O. Box 450228, Dubai, United Arab Emirates<br>
				Telephone: +971 4 244 0640 / 800 PJRE [7573]<br>
				FAX : +971 4 244 0806 Mobile: +971 52 929 1221<br>
			</h3>
			<!-- <h3>For all inquiries: office@translate4africa.com<br>
			Office: Plot 328, Kisaasi Road Ntinda, Kampala, Uganda.<br>
			Tel: +256788104221</h3> -->
		</div>
	</div>
</div>
<?php include 'inc/footer.php'; ?>