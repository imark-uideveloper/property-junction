document.documentElement.addEventListener('touchstart', function(event) {
	if (event.touches.length > 1) {
		event.preventDefault();
	}
}, false);

jQuery(document).ready(function($) {
	function loadr() {}

	function loadresize() {
		var a = $(window).outerHeight();
		var b = $('.coming-soon').outerHeight();
		if( b <= a ) {
			$('.coming-soon').css('min-height', a);
			$('.coming-soon').removeClass('coming-fix');
		}
		else {
			$('.coming-soon').addClass('coming-fix');
			$('.coming-soon').css('min-height', a);
		}
	}

	function mob() {}

	function tabport() {}

	function tabland() {}

	function desk() {}

	function rsize() {
		var a = $(window).width();
		if ( a > 0 && a < 768)
			mob();
		if ( a > 0 && a < 992 )
			tabport();
		if ( a > 0 && a < 1200)
			tabland();
		if ( a > 1200)
			desk();
	}

	loadr();
	// loadresize();
	$(window).on('resize load', function() {
		loadresize();
		rsize();
	});
});
