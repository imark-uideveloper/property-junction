<?php include 'include/header.php'; ?>
<div class="pageContainer innerPage creers">
	<section class="banner innerBanner" style="background-image: url(images/innerBanner.jpg);">
		<div class="bannerContainer">
			<div class="bannerWrapper">
				<div class="container">
					<div class="bannerInner">
						<h1>Services</h1>
						<p><a href="index">Home</a><span>/</span><span>Services</span></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd">
		<div class="container">
			<h3>Re-Sales / Secondary Market Service</h3>
			<p>The re-sale/secondary market usually attracts the end-user, a client buying to live in the property as their main home, or as a second holiday home. Our agents are trained to qualify the client to assess their needs, and will provide a full information pack identifying the projects and properties that match their requirements and expectations. This pack will include information on educational centers, hospitals, leisure amenities and other criteria that our agents identify during their personal consultation. Our multi-lingual team of sales professionals provide the highest possible service for this segment of the market.</p>

			<h3>Investment Advisory Service</h3>
			<p>Our investment professionals will advise and educate investment clients (as opposed to end-users) on Dubai, the property market, historical projects, and return on investment. The team provides a range of professional services to advise clients on real estate investments, including specialist research and on-going market conditions, identifying investment scenarios and portfolio diversification solutions. Our investor clients will receive a full report to qualify the agent’s recommendations, including comparative case studies and comparative market analysis reports.</p>

			<h3>Portfolio Management Service</h3>
			<p>Property Junction has an enviable reputation in the Dubai property market and our experience and extensive knowledge gained of the entire property market is simply unmatched. Investment clientele benefit from our expertise of the realities of market returns and risks. Charged with making investment decisions in today’s competitive environment, it is crucial that our portfolio management consultants are extensively trained and ‘selected’ for their understanding of the fundamentals of property portfolio management.</p>

			<h3>Open House</h3>
			<p>Our Open House service is an extremely popular facility, and is well established in Dubai. Our agents provide a very informative experience to visitors of their ‘Open Houses’, providing local area information and comparative market analysis to help the potential buyer make the most informed decision when buying a property.</p>

			<h3>Property Listing Service</h3>
			<p>Listing a property in an effective, professional manner is of paramount importance. This is a service we take very seriously. We can provide a dedicated senior account manager to visit you, by appointment, to carry out a full market appraisal of your property. This will include an overview of Property Junction and demonstration of the range of marketing services provided. Our Sellers are encouraged to contribute to the marketing plan for their property.</p>

			<h3>The range of marketing collateral includes:</h3>
			<ul>
				<li>Latest website technology – listings updated daily.</li>
				<li>Listings and photography within local and regional, daily, weekly and monthly media.</li>
				<li>Targeted digital marketing – potentially 50,000+ filtered database.</li>
				<li>Network of registered local agents with pre-approved and qualified buyers looking for specific properties.</li>
				<li>Individual property cards created to maximize key features and ‘fast facts’.</li>
				<li>Open house option – to maximize exposure.</li>
				<li>Dedicated sales account managers, who act as primary contact.</li>
				<li>Photographs of the property to ensure best features are identified.</li>
				<li>Individual valuation – every property is unique and treated individually.</li>
			</ul>

			<h3>Property Rentals</h3>
			<p>We have a dedicated team specializing in the rental market, providing extensive knowledge of all areas, including the location of schools, medical services, financial districts, leisure facilities and amenities. Landlords are given excellent presentation and maximum exposure of their properties, achieving the best possible rental income. We have a constant flow of private and corporate tenants listed at any one time, most looking to move quickly. Using the latest CRM and website technology, the Property Junction rental staff are able to market landlord’s properties for rent in the most sophisticated manner – producing instant, high quality property presentations to send direct to prospective tenants.</p>
			<p>Our highly trained and experienced staff offers the best of customer care. For landlords, that translates to dedicated staff with complete knowledge of the property and the surrounding areas. Whether our tenants are looking for a cosmopolitan style penthouse in Dubai Marina, a quiet home in the suburbs or a front line golf villa, we will find that special place to call home. Our tenants are provided a dedicated account manager who will provide an orientation tour and services including research into schools, medical centres, leisure facilities, accessibility to work-place and travel times.</p>

			<h3>Personal Consultation and Advisory Service</h3>
			<p>This service has proved especially popular during the past 2 years, when both investor and end-user confidence has been at an all-time low. Clients recognizing our extensive market knowledge can take comfort from a small team dedicated to meeting with them at our head office to discuss their needs, requirements and of course financial circumstances. Whilst many purchases have gone ahead, not all clients are advised to proceed with buying a property, or continuing along the investment path they were considering, if the circumstances do not favour them. It is our belief that this honest and transparent approach will win us return business from these clients, when the time is right.</p>
		</div>
	</section>
</div>
<?php include 'include/footer.php'; ?>