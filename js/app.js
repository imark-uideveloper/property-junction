jQuery(document).ready(function() {
	/* Custom Scroll jQuery */
	jQuery(window).on("load",function(){
		jQuery("body").mCustomScrollbar({
			callbacks:{
				whileScrolling:function(){
					var bodyScroll = (this.mcs.top) * -1,
					headerPos = jQuery('header').outerHeight();
					if (innerPage) {
						var headerHeight = jQuery('header').outerHeight();
						jQuery('body').addClass('innerPage');
						if (bodyScroll < 5) {
							jQuery('body').removeClass('fixedHeader');
							jQuery('.pageContainer.innerPage').css('padding-top', 0);
						} else {
							jQuery('body').addClass('fixedHeader');
							jQuery('.pageContainer.innerPage').css('padding-top', headerHeight);
						}
					} else {
						if (bodyScroll < headerPos) {
							jQuery('body').removeClass('fixedHeader');
						} else {
							jQuery('body').addClass('fixedHeader');
						}
					}
				}
			},
			mouseWheelPixels: 200,
		});
	});
	/* Custom Scroll jQuery */

	/* Active Current Menu Item jQuery */
	var a = window.location.pathname;
	jQuery('.mainNav li').each(function(i) {
		var b = jQuery(this).find('a').attr('href');
		if (a.indexOf(b) > -1) {
			jQuery(this).addClass('current-menu-item');
			jQuery(this).siblings().removeClass('current-menu-item');

			var dropdown = jQuery(this).parent().hasClass('dropdown-menu');
			if(dropdown) {
				jQuery(this).parent().parent().addClass('current-menu-item');
			}
		} else {
			jQuery(this).removeClass('current-menu-item');
		}
		if ((window.location.origin + "/propertyjunction/").indexOf(a) > -1) {
			jQuery('.mainNav li').first().addClass('current-menu-item');
		}
	});
	/* Active Current Menu Item jQuery */

	/* First Letter Capital */
	jQuery('.firstCap').on('keypress', function(event) {
		var jQuerythis = jQuery(this),
		thisVal = jQuerythis.val(),
		FLC = thisVal.slice(0, 1).toUpperCase();
		con = thisVal.slice(1, thisVal.length);
		jQuery(this).val(FLC + con);
	});

	jQuery(".rangeSlider").each(function() {
		var thisMin = jQuery(this).data('min'),
			thisMax = jQuery(this).data('max'),
			thisRange = jQuery(this).data('range'),
			thisval = jQuery(this).data('val');
		jQuery(this).slider({ id: "slider12c", min: thisMin, max: thisMax, range: thisRange, value: thisval }).data('slider');
	});

	jQuery('#agentSlider').owlCarousel({
		dots: true,
		loop: true,
		margin: 30,
		nav: false,
		responsive:{
			0:{
				items: 1
			},
			600:{
				items: 2
			},
			1000:{
				items: 4
			}
		}
	});

	jQuery('#latestSlider').owlCarousel({
		dots: false,
		loop: true,
		margin: 30,
		nav: true,
		responsive:{
			0:{
				items: 1
			},
			600:{
				items: 2
			},
			1000:{
				items: 4
			}
		}
	});

	jQuery('#testimonialSlider').owlCarousel({
		dots: true,
		items: 1,
		loop: true,
		margin: 0,
		nav: false
	});

	jQuery('.sideInner .owl-carousel').owlCarousel({
		autoplay: true,
		autoplayTimeout: 3000,
		dots: true,
		items: 1,
		loop: true,
		margin: 0,
		nav: true
	});

	var innerPage = jQuery('.pageContainer').hasClass('innerPage');
	if (innerPage) {
		var headerHeight = jQuery('header').outerHeight();
		jQuery('body').addClass('innerPage');
	}

	jQuery('.dataLink').on('click', function(event) {
		event.preventDefault();
		var thisLink = jQuery(this).data('link');
		jQuery(this).addClass('active').parent().siblings().find('a').removeClass('active');
		jQuery('.dataTab[ data-tab="' + thisLink + '"]').siblings().removeClass('active');
		jQuery('.dataTab[ data-tab="' + thisLink + '"]').addClass('active');
	});


	// Slider
	jQuery(window).load(function() {
		/* Act on the event */
		if(jQuery('.product__slider-main').length) {
			var $slider = jQuery('.product__slider-main').on('init', function(slick) {
				jQuery('.product__slider-main').fadeIn(1000);
			}).slick({
				arrows: false,
				asNavFor: '.product__slider-thmb',
				autoplay: true,
				autoplaySpeed: 3000,
				fade: true,
				lazyLoad: 'ondemand',
				slidesToScroll: 1,
				slidesToShow: 1,
			});

			var $slider2 = jQuery('.product__slider-thmb').on('init', function(slick) {
				jQuery('.product__slider-thmb').fadeIn(1000);
			}).slick({
				arrows: false,
				asNavFor: '.product__slider-main',
				centerMode: false,
				dots: false,
				focusOnSelect: true,
				lazyLoad: 'ondemand',
				slidesToShow: 7,
				slidesToScroll: 1,
			});

			//remove active class from all thumbnail slides
			jQuery('.product__slider-thmb .slick-slide').removeClass('slick-current');

			//set active class to first thumbnail slides
			jQuery('.product__slider-thmb .slick-slide').eq(0).addClass('slick-current');

			// On before slide change match active thumbnail to current slide
			jQuery('.product__slider-main').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
				var mySlideNumber = nextSlide;
				jQuery('.product__slider-thmb .slick-slide').removeClass('slick-current');
				jQuery('.product__slider-thmb .slick-slide').eq(mySlideNumber).addClass('slick-current');
			});

			// slick slider options
			// see: https://kenwheeler.github.io/slick/
			var sliderOptions = {
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				fade: true,
				autoplay: true
			}

			// slick slider options
			// see: https://kenwheeler.github.io/slick/
			var previewSliderOptions = {
				slidesToShow: 3,
				slidesToScroll: 1,
				dots: false,
				focusOnSelect: true,
				centerMode: true
			}
		}
	});
});