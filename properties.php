<?php include 'include/header.php'; ?>
<div class="pageContainer innerPage property">
	<section class="banner innerBanner" style="background-image: url(images/innerBanner.jpg);">
		<div class="bannerContainer">
			<div class="bannerWrapper">
				<div class="container">
					<div class="bannerInner">
						<h1>Properties</h1>
						<p><a href="index">Home</a><span>/</span><span>Properties</span></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd bgGray">
		<div class="container">
			<h2 class="hide"> </h2>
			<div class="propertiesListType">
			<ul>
				<li>
					<a href="javascript:void(0)" class="dataLink active" title="All" data-link="All">All</a>
				</li>
				<li>
					<a href="javascript:void(0)" class="dataLink" title="For Sale" data-link="ForSale">For Sale</a>
				</li>
				<li>
					<a href="javascript:void(0)" class="dataLink" title="For Rent" data-link="ForRent">For Rent</a>
				</li>
			</ul>
			</div>

			<div class="row">
				<div class="col-sm-9">
					<div class="propertiesList">
						<div class="dataTab active" data-tab="All">
							<ul>
								<li>
									<div class="featuredPropertyWrapper">
										<div class="proImag">
											<figure style="background-image: url(images/propertyImg1.jpg)"></figure>
										</div>
										<div class="proInner">
											<div class="proInnerContainer">
												<div class="proInnerWrapper">
													<div class="proHead">
														<h4><a href="propertyDetailes.php">FULLY FURNISHED 2-Bedroom Apartment Available For Rent in Dubai Marina - 15,000 for 1 month</a></h4>
														<h3>Dubai Marina</h3></div>
													<div class="proCon">Property Junction Real Estate Is Proud To Offer You Fully Serviced and Furnished Spacious Deluxe 2-Bedroom Apartment In Dubai Marina for rent.</div>
													<div class="proFooter">
														<div class="area">Area: 968.75</div>
														<div class="fatr">
															<ul>
																<li>
																	<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
																		<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
																		<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
																		<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
																	</svg>
																	<span>2</span>
																</li>
																<li>
																	<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
																		<g>
																			<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
																		</g>
																	</svg>
																	<span>3</span>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<div class="proPrice">
													<div class="propertyPrice">
														<span>
															<span>AED 15,000</span>
															<span>AED 1,800/sq ft</span>
														</span>
													</div>
													<div class="propertyPrice">
														<a class="queryBtn" href="javascript:void(0)">Send Enquiry</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="featuredPropertyWrapper">
										<div class="proImag">
											<figure style="background-image: url(images/propertyImg2.jpg)"></figure>
										</div>
										<div class="proInner">
											<div class="proInnerContainer">
												<div class="proInnerWrapper">
													<div class="proHead">
														<h4><a href="propertyDetailes.php">FULLY FURNISHED 2-Bedroom Apartment Available For Rent in Dubai Marina - 15,000 for 1 month</a></h4>
														<h3>Dubai Marina</h3></div>
													<div class="proCon">Property Junction Real Estate Is Proud To Offer You Fully Serviced and Furnished Spacious Deluxe 2-Bedroom Apartment In Dubai Marina for rent.</div>
													<div class="proFooter">
														<div class="area">Area: 968.75</div>
														<div class="fatr">
															<ul>
																<li>
																	<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
																		<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
																		<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
																		<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
																	</svg>
																	<span>2</span>
																</li>
																<li>
																	<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
																		<g>
																			<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
																		</g>
																	</svg>
																	<span>3</span>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<div class="proPrice">
													<div class="propertyPrice">
														<span>
															<span>AED 15,000</span>
															<span>AED 1,800/sq ft</span>
														</span>
													</div>
													<div class="propertyPrice">
														<a class="queryBtn" href="javascript:void(0)">Send Enquiry</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="featuredPropertyWrapper">
										<div class="proImag">
											<figure style="background-image: url(images/propertyImg3.jpg)"></figure>
										</div>
										<div class="proInner">
											<div class="proInnerContainer">
												<div class="proInnerWrapper">
													<div class="proHead">
														<h4><a href="propertyDetailes.php">FULLY FURNISHED 2-Bedroom Apartment Available For Rent in Dubai Marina - 15,000 for 1 month</a></h4>
														<h3>Dubai Marina</h3></div>
													<div class="proCon">Property Junction Real Estate Is Proud To Offer You Fully Serviced and Furnished Spacious Deluxe 2-Bedroom Apartment In Dubai Marina for rent.</div>
													<div class="proFooter">
														<div class="area">Area: 968.75</div>
														<div class="fatr">
															<ul>
																<li>
																	<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
																		<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
																		<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
																		<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
																	</svg>
																	<span>2</span>
																</li>
																<li>
																	<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
																		<g>
																			<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
																		</g>
																	</svg>
																	<span>3</span>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<div class="proPrice">
													<div class="propertyPrice">
														<span>
															<span>AED 15,000</span>
															<span>AED 1,800/sq ft</span>
														</span>
													</div>
													<div class="propertyPrice">
														<a class="queryBtn" href="javascript:void(0)">Send Enquiry</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="featuredPropertyWrapper">
										<div class="proImag">
											<figure style="background-image: url(images/propertyImg4.jpg)"></figure>
										</div>
										<div class="proInner">
											<div class="proInnerContainer">
												<div class="proInnerWrapper">
													<div class="proHead">
														<h4><a href="propertyDetailes.php">FULLY FURNISHED 2-Bedroom Apartment Available For Rent in Dubai Marina - 15,000 for 1 month</a></h4>
														<h3>Dubai Marina</h3></div>
													<div class="proCon">Property Junction Real Estate Is Proud To Offer You Fully Serviced and Furnished Spacious Deluxe 2-Bedroom Apartment In Dubai Marina for rent.</div>
													<div class="proFooter">
														<div class="area">Area: 968.75</div>
														<div class="fatr">
															<ul>
																<li>
																	<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
																		<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
																		<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
																		<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
																	</svg>
																	<span>2</span>
																</li>
																<li>
																	<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
																		<g>
																			<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
																		</g>
																	</svg>
																	<span>3</span>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<div class="proPrice">
													<div class="propertyPrice">
														<span>
															<span>AED 15,000</span>
															<span>AED 1,800/sq ft</span>
														</span>
													</div>
													<div class="propertyPrice">
														<a class="queryBtn" href="javascript:void(0)">Send Enquiry</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="featuredPropertyWrapper">
										<div class="proImag">
											<figure style="background-image: url(images/propertyImg5.jpg)"></figure>
										</div>
										<div class="proInner">
											<div class="proInnerContainer">
												<div class="proInnerWrapper">
													<div class="proHead">
														<h4><a href="propertyDetailes.php">FULLY FURNISHED 2-Bedroom Apartment Available For Rent in Dubai Marina - 15,000 for 1 month</a></h4>
														<h3>Dubai Marina</h3></div>
													<div class="proCon">Property Junction Real Estate Is Proud To Offer You Fully Serviced and Furnished Spacious Deluxe 2-Bedroom Apartment In Dubai Marina for rent.</div>
													<div class="proFooter">
														<div class="area">Area: 968.75</div>
														<div class="fatr">
															<ul>
																<li>
																	<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
																		<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
																		<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
																		<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
																	</svg>
																	<span>2</span>
																</li>
																<li>
																	<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
																		<g>
																			<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
																		</g>
																	</svg>
																	<span>3</span>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<div class="proPrice">
													<div class="propertyPrice">
														<span>
															<span>AED 15,000</span>
															<span>AED 1,800/sq ft</span>
														</span>
													</div>
													<div class="propertyPrice">
														<a class="queryBtn" href="javascript:void(0)">Send Enquiry</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="featuredPropertyWrapper">
										<div class="proImag">
											<figure style="background-image: url(images/propertyImg6.jpg)"></figure>
										</div>
										<div class="proInner">
											<div class="proInnerContainer">
												<div class="proInnerWrapper">
													<div class="proHead">
														<h4><a href="propertyDetailes.php">FULLY FURNISHED 2-Bedroom Apartment Available For Rent in Dubai Marina - 15,000 for 1 month</a></h4>
														<h3>Dubai Marina</h3></div>
													<div class="proCon">Property Junction Real Estate Is Proud To Offer You Fully Serviced and Furnished Spacious Deluxe 2-Bedroom Apartment In Dubai Marina for rent.</div>
													<div class="proFooter">
														<div class="area">Area: 968.75</div>
														<div class="fatr">
															<ul>
																<li>
																	<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
																		<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
																		<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
																		<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
																	</svg>
																	<span>2</span>
																</li>
																<li>
																	<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
																		<g>
																			<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
																		</g>
																	</svg>
																	<span>3</span>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<div class="proPrice">
													<div class="propertyPrice">
														<span>
															<span>AED 15,000</span>
															<span>AED 1,800/sq ft</span>
														</span>
													</div>
													<div class="propertyPrice">
														<a class="queryBtn" href="javascript:void(0)">Send Enquiry</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="featuredPropertyWrapper">
										<div class="proImag">
											<figure style="background-image: url(images/propertyImg7.jpg)"></figure>
										</div>
										<div class="proInner">
											<div class="proInnerContainer">
												<div class="proInnerWrapper">
													<div class="proHead">
														<h4><a href="propertyDetailes.php">FULLY FURNISHED 2-Bedroom Apartment Available For Rent in Dubai Marina - 15,000 for 1 month</a></h4>
														<h3>Dubai Marina</h3></div>
													<div class="proCon">Property Junction Real Estate Is Proud To Offer You Fully Serviced and Furnished Spacious Deluxe 2-Bedroom Apartment In Dubai Marina for rent.</div>
													<div class="proFooter">
														<div class="area">Area: 968.75</div>
														<div class="fatr">
															<ul>
																<li>
																	<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
																		<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
																		<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
																		<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
																	</svg>
																	<span>2</span>
																</li>
																<li>
																	<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
																		<g>
																			<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
																		</g>
																	</svg>
																	<span>3</span>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<div class="proPrice">
													<div class="propertyPrice">
														<span>
															<span>AED 15,000</span>
															<span>AED 1,800/sq ft</span>
														</span>
													</div>
													<div class="propertyPrice">
														<a class="queryBtn" href="javascript:void(0)">Send Enquiry</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="featuredPropertyWrapper">
										<div class="proImag">
											<figure style="background-image: url(images/propertyImg8.jpg)"></figure>
										</div>
										<div class="proInner">
											<div class="proInnerContainer">
												<div class="proInnerWrapper">
													<div class="proHead">
														<h4><a href="propertyDetailes.php">FULLY FURNISHED 2-Bedroom Apartment Available For Rent in Dubai Marina - 15,000 for 1 month</a></h4>
														<h3>Dubai Marina</h3></div>
													<div class="proCon">Property Junction Real Estate Is Proud To Offer You Fully Serviced and Furnished Spacious Deluxe 2-Bedroom Apartment In Dubai Marina for rent.</div>
													<div class="proFooter">
														<div class="area">Area: 968.75</div>
														<div class="fatr">
															<ul>
																<li>
																	<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
																		<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
																		<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
																		<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
																	</svg>
																	<span>2</span>
																</li>
																<li>
																	<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
																		<g>
																			<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
																		</g>
																	</svg>
																	<span>3</span>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<div class="proPrice">
													<div class="propertyPrice">
														<span>
															<span>AED 15,000</span>
															<span>AED 1,800/sq ft</span>
														</span>
													</div>
													<div class="propertyPrice">
														<a class="queryBtn" href="javascript:void(0)">Send Enquiry</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="featuredPropertyWrapper">
										<div class="proImag">
											<figure style="background-image: url(images/propertyImg9.jpg)"></figure>
										</div>
										<div class="proInner">
											<div class="proInnerContainer">
												<div class="proInnerWrapper">
													<div class="proHead">
														<h4><a href="propertyDetailes.php">FULLY FURNISHED 2-Bedroom Apartment Available For Rent in Dubai Marina - 15,000 for 1 month</a></h4>
														<h3>Dubai Marina</h3></div>
													<div class="proCon">Property Junction Real Estate Is Proud To Offer You Fully Serviced and Furnished Spacious Deluxe 2-Bedroom Apartment In Dubai Marina for rent.</div>
													<div class="proFooter">
														<div class="area">Area: 968.75</div>
														<div class="fatr">
															<ul>
																<li>
																	<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
																		<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
																		<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
																		<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
																	</svg>
																	<span>2</span>
																</li>
																<li>
																	<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
																		<g>
																			<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
																		</g>
																	</svg>
																	<span>3</span>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<div class="proPrice">
													<div class="propertyPrice">
														<span>
															<span>AED 15,000</span>
															<span>AED 1,800/sq ft</span>
														</span>
													</div>
													<div class="propertyPrice">
														<a class="queryBtn" href="javascript:void(0)">Send Enquiry</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="featuredPropertyWrapper">
										<div class="proImag">
											<figure style="background-image: url(images/propertyImg10.jpg)"></figure>
										</div>
										<div class="proInner">
											<div class="proInnerContainer">
												<div class="proInnerWrapper">
													<div class="proHead">
														<h4><a href="propertyDetailes.php">FULLY FURNISHED 2-Bedroom Apartment Available For Rent in Dubai Marina - 15,000 for 1 month</a></h4>
														<h3>Dubai Marina</h3></div>
													<div class="proCon">Property Junction Real Estate Is Proud To Offer You Fully Serviced and Furnished Spacious Deluxe 2-Bedroom Apartment In Dubai Marina for rent.</div>
													<div class="proFooter">
														<div class="area">Area: 968.75</div>
														<div class="fatr">
															<ul>
																<li>
																	<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
																		<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
																		<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
																		<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
																	</svg>
																	<span>2</span>
																</li>
																<li>
																	<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
																		<g>
																			<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
																		</g>
																	</svg>
																	<span>3</span>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<div class="proPrice">
													<div class="propertyPrice">
														<span>
															<span>AED 15,000</span>
															<span>AED 1,800/sq ft</span>
														</span>
													</div>
													<div class="propertyPrice">
														<a class="queryBtn" href="javascript:void(0)">Send Enquiry</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="featuredPropertyWrapper">
										<div class="proImag">
											<figure style="background-image: url(images/propertyImg11.jpg)"></figure>
										</div>
										<div class="proInner">
											<div class="proInnerContainer">
												<div class="proInnerWrapper">
													<div class="proHead">
														<h4><a href="propertyDetailes.php">FULLY FURNISHED 2-Bedroom Apartment Available For Rent in Dubai Marina - 15,000 for 1 month</a></h4>
														<h3>Dubai Marina</h3></div>
													<div class="proCon">Property Junction Real Estate Is Proud To Offer You Fully Serviced and Furnished Spacious Deluxe 2-Bedroom Apartment In Dubai Marina for rent.</div>
													<div class="proFooter">
														<div class="area">Area: 968.75</div>
														<div class="fatr">
															<ul>
																<li>
																	<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
																		<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
																		<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
																		<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
																	</svg>
																	<span>2</span>
																</li>
																<li>
																	<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
																		<g>
																			<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
																		</g>
																	</svg>
																	<span>3</span>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<div class="proPrice">
													<div class="propertyPrice">
														<span>
															<span>AED 15,000</span>
															<span>AED 1,800/sq ft</span>
														</span>
													</div>
													<div class="propertyPrice">
														<a class="queryBtn" href="javascript:void(0)">Send Enquiry</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="featuredPropertyWrapper">
										<div class="proImag">
											<figure style="background-image: url(images/propertyImg12.jpg)"></figure>
										</div>
										<div class="proInner">
											<div class="proInnerContainer">
												<div class="proInnerWrapper">
													<div class="proHead">
														<h4><a href="propertyDetailes.php">FULLY FURNISHED 2-Bedroom Apartment Available For Rent in Dubai Marina - 15,000 for 1 month</a></h4>
														<h3>Dubai Marina</h3></div>
													<div class="proCon">Property Junction Real Estate Is Proud To Offer You Fully Serviced and Furnished Spacious Deluxe 2-Bedroom Apartment In Dubai Marina for rent.</div>
													<div class="proFooter">
														<div class="area">Area: 968.75</div>
														<div class="fatr">
															<ul>
																<li>
																	<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
																		<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
																		<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
																		<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
																	</svg><span>2</span>
																</li>
																<li>
																	<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
																		<g>
																			<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
																		</g>
																	</svg><span>3</span>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<div class="proPrice">
													<div class="propertyPrice">
														<span>
															<span>AED 15,000</span>
															<span>AED 1,800/sq ft</span>
														</span>
													</div>
													<div class="propertyPrice">
														<a class="queryBtn" href="javascript:void(0)">Send Enquiry</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="featuredPropertyWrapper">
										<div class="proImag">
											<figure style="background-image: url(images/propertyImg13.jpg)"></figure>
										</div>
										<div class="proInner">
											<div class="proInnerContainer">
												<div class="proInnerWrapper">
													<div class="proHead">
														<h4><a href="propertyDetailes.php">FULLY FURNISHED 2-Bedroom Apartment Available For Rent in Dubai Marina - 15,000 for 1 month</a></h4>
														<h3>Dubai Marina</h3></div>
													<div class="proCon">Property Junction Real Estate Is Proud To Offer You Fully Serviced and Furnished Spacious Deluxe 2-Bedroom Apartment In Dubai Marina for rent.</div>
													<div class="proFooter">
														<div class="area">Area: 968.75</div>
														<div class="fatr">
															<ul>
																<li>
																	<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
																		<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
																		<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
																		<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
																		<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
																		<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
																		<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
																		<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
																	</svg>
																	<span>2</span>
																</li>
																<li>
																	<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
																		<g>
																			<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
																		</g>
																	</svg>
																	<span>3</span>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<div class="proPrice">
													<div class="propertyPrice">
														<span>
															<span>AED 15,000</span>
															<span>AED 1,800/sq ft</span>
														</span>
													</div>
													<div class="propertyPrice">
														<a class="queryBtn" href="javascript:void(0)">Send Enquiry</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<div class="dataTab" data-tab="ForSale"></div>
						<div class="dataTab" data-tab="ForRent"></div>
					</div>
				</div>
				<div class="col-sm-3 sidebarContainer">
					<div class="formContainer sideInner">
						<h4>Find Your Home</h4>
						<form >
							<div class="inputContainer">
								<ul>
									<li>
										<div class="form-group">
											<select class="form-control" name="Emirate" id="Emirate">
												<option value="0">Emirate</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
											</select>
											<label for="Emirate" class="caret"></label>
										</div>
									</li>
									<li>
										<div class="form-group">
											<select class="form-control" name="Location" id="Location">
												<option value="0">Location</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
											</select>
											<label for="Location" class="caret"></label>
										</div>
									</li>
									<li>
										<div class="form-group">
											<select class="form-control" name="SubLocation" id="SubLocation">
												<option value="0">Sub Location</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
											</select>
											<label for="SubLocation" class="caret"></label>
										</div>
									</li>
									<li>
										<div class="form-group">
											<select class="form-control" name="PropertyType" id="PropertyType">
												<option value="0">Property Type</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
											</select>
											<label for="PropertyType" class="caret"></label>
										</div>
									</li>
									<li>
										<div class="form-group">
											<select class="form-control" name="Bed" id="Bed">
												<option value="0">Beds</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
											</select>
											<label for="Bed" class="caret"></label>
										</div>
									</li>
									<li>
										<div class="form-group">
											<select class="form-control" name="bath" id="bath">
												<option value="0">Baths</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
											</select>
											<label for="bath" class="caret"></label>
										</div>
									</li>
									<li>
										<div class="form-group">
											<label>10k (AED) -  20000k (AED)</label>
											<div data-role="rangeslider">
												<input class="rangeSlider" data-min="10" data-max="20000" data-range="true" data-val="[10, 20000]" data-slider-handle="square" type="text"/>
											</div>
										</div>
									</li>
									<li>
										<div class="form-group">
											<select class="form-control" name="Short" id="Short">
												<option value="0">Short By</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
											</select>
											<label for="Short" class="caret"></label>
										</div>
									</li>
									<li>
										<div class="form-group">
											<select class="form-control" name="Order" id="Order">
												<option value="0">Order</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
											</select>
											<label for="Order" class="caret"></label>
										</div>
									</li>
									<li>
										<div class="form-group">
											<input type="submit" name="sub" value="Search Listing">
										</div>
									</li>
								</ul>
							</div>
						</form>
					</div>

					<div class="sideInner">
						<h4>Property Status</h4>

						<div class="prostatus">
							<ul>
								<li>
									<div>
										<a href="javascript:void(0)">For Rent</a>
									</div>
									<div class="proCounter">
										<span>(200)</span>
									</div>
								</li>
								<li>
									<div>
										<a href="javascript:void(0)">For Sale</a>
									</div>
									<div class="proCounter">
										<span>(70)</span>
									</div>
								</li>
							</ul>
						</div>
					</div>

					<div class="sideInner">
						<h4>Featured Properties</h4>

						<div class="owl-carousel owl-theme">
							<div class="item">
								<figure style="background-image: url('images/proImg7.jpg')">
									<span>ADE 1,900/sq ft</span>
								</figure>
							</div>
							<div class="item">
								<figure style="background-image: url('images/proImg7.jpg')">
									<span>ADE 1,900/sq ft</span>
								</figure>
							</div>
							<div class="item">
								<figure style="background-image: url('images/proImg7.jpg')">
									<span>ADE 1,900/sq ft</span>
								</figure>
							</div>
							<div class="item">
								<figure style="background-image: url('images/proImg7.jpg')">
									<span>ADE 1,900/sq ft</span>
								</figure>
							</div>
							<div class="item">
								<figure style="background-image: url('images/proImg7.jpg')">
									<span>ADE 1,900/sq ft</span>
								</figure>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php include 'include/footer.php'; ?>