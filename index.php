<?php include 'include/header.php'; ?>
<div class="pageContainer home index">
	<section class="banner">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active" style="background-image: url('images/banner.jpg');">
					<div class="bannerContainer">
						<div class="bannerWrapper">
							<div class="container">
								<div class="bannerInner">
									<h1>Luxury Properties <span>For Sale</span></h1>
									<p>Exclusive luxury residential specialists</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item" style="background-image: url('images/banner.jpg');">
					<div class="bannerContainer">
						<div class="bannerWrapper">
							<div class="container">
								<div class="bannerInner">
									<h1>Luxury Properties <span>For Sale</span></h1>
									<p>Exclusive luxury residential specialists</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item" style="background-image: url('images/banner.jpg');">
					<div class="bannerContainer">
						<div class="bannerWrapper">
							<div class="container">
								<div class="bannerInner">
									<h1>Luxury Properties <span>For Sale</span></h1>
									<p>Exclusive luxury residential specialists</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="container">
				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
					<img src="images/sliderImgRight.png" alt="Slider Arrow">
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
					<img src="images/sliderImgLeft.png" alt="Slider Arrow">
					<!-- <img src="images/sliderImgLeft.png" alt="Slider Arrow"> -->
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</section>

	<section class="searchForm bgGray">
		<div class="container">
			<h2 class="hide"> </h2>
			<div class="searchWrapper">
				<div class="searchType">
					<ul>
						<li><a class="active dataLink" href="#" data-link="Rent">Rent</a></li>
						<li><a class="dataLink" href="#" data-link="Sale">Sale</a></li>
					</ul>
				</div>
				<div class="formContainer">
					<form >
						<div class="inputContainer dataTab active" data-tab="Rent">
							<ul>
								<li>
									<div class="form-group">
										<select class="form-control" name="Emirate" id="Emirate">
											<option value="0">Any Emirate</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
										<label for="Emirate" class="caret"></label>
									</div>
								</li>
								<li>
									<div class="form-group">
										<select class="form-control" name="Location" id="Location">
											<option value="0">Any Location</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
										<label for="Location" class="caret"></label>
									</div>
								</li>
								<li>
									<div class="form-group">
										<select class="form-control" name="SubLocation" id="SubLocation">
											<option value="0">Any Sub Location</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
										<label for="SubLocation" class="caret"></label>
									</div>
								</li>
								<li>
									<div class="form-group">
										<select class="form-control" name="PropertyType" id="PropertyType">
											<option value="0">Any Property Type</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
										<label for="PropertyType" class="caret"></label>
									</div>
								</li>
								<li>
									<div class="form-group">
										<select class="form-control" name="Bed" id="Bed">
											<option value="0">Any Bed</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
										<label for="Bed" class="caret"></label>
									</div>
								</li>
								<li>
									<div class="form-group">
										<select class="form-control" name="bath" id="bath">
											<option value="0">Any Bath</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
										<label for="bath" class="caret"></label>
									</div>
								</li>
								<li>
									<div class="form-group">
										<label>10k (AED) -  100k (AED)</label>
										<div data-role="rangeslider">
											<input class="rangeSlider" data-min="10" data-max="100" data-range="true" data-val="[10, 100]" data-slider-handle="square" type="text"/>
										</div>
									</div>
								</li>
								<li>
									<div class="form-group">
										<input type="submit" name="sub" value="Search Listings">
									</div>
								</li>
							</ul>
						</div>
						<div class="inputContainer dataTab" data-tab="Sale">
							<ul>
								<li>
									<div class="form-group">
										<select class="form-control" name="Emirate1" id="Emirate1">
											<option value="0">Any Emirate</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
										<label for="Emirate" class="caret"></label>
									</div>
								</li>
								<li>
									<div class="form-group">
										<select class="form-control" name="Location1" id="Location1">
											<option value="0">Any Location</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
										<label for="Location" class="caret"></label>
									</div>
								</li>
								<li>
									<div class="form-group">
										<select class="form-control" name="SubLocation1" id="SubLocation1">
											<option value="0">Any Sub Location</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
										<label for="SubLocation" class="caret"></label>
									</div>
								</li>
								<li>
									<div class="form-group">
										<select class="form-control" name="PropertyType1" id="PropertyType1">
											<option value="0">Any Property Type</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
										<label for="PropertyType" class="caret"></label>
									</div>
								</li>
								<li>
									<div class="form-group">
										<select class="form-control" name="Bed1" id="Bed1">
											<option value="0">Any Bed</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
										<label for="Bed" class="caret"></label>
									</div>
								</li>
								<li>
									<div class="form-group">
										<select class="form-control" name="bath1" id="bath1">
											<option value="0">Any Bath</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
										<label for="bath" class="caret"></label>
									</div>
								</li>
								<li>
									<div class="form-group">
										<label>10k (AED) -  100k (AED)</label>
										<div data-role="rangeslider">
											<input class="rangeSlider" data-min="10" data-max="100" data-range="true" data-val="[10, 100]" data-slider-handle="square" type="text"/>
										</div>
									</div>
								</li>
								<li>
									<div class="form-group">
										<input type="submit" name="sub" value="Search Listings">
									</div>
								</li>
							</ul>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd featuredProperty bgGray">
		<div class="container">
			<h2 class="title">Featured Properties <span>Check out some of our listed properties</span></h2>

			<div class="featuredPropertyList">
				<div class="row">
					<div class="col-md-3">
						<div class="featuredPropertyWrapper">
							<div class="proImag">
								<figure style="background-image: url(images/proImg1.jpg)"></figure>
							</div>
							<div class="proInner">
								<div class="proHead">
									<h4><a href="propertyDetailes.php">Vacant Full Lake View Upgraded Unit</a></h4>
								</div>
								<div class="proCon">Property Junction is Proud to Offer You for Sale This 2 Bedroom 
								Apartment in Lake View  - JLT</div>
								<div class="proFooter">
									<div class="area"><strong>Area:</strong> 1500</div>
									<div class="fatr">
										<ul>
											<li>
												<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
													<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
													<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
													<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
												</svg>
												<span>2</span>
											</li>
											<li>
												<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
													<g>
														<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
													</g>
												</svg>
												<span>3</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="featuredPropertyWrapper">
							<div class="proImag">
								<figure style="background-image: url(images/proImg2.jpg)"></figure>
							</div>
							<div class="proInner">
								<div class="proHead">
									<h4><a href="propertyDetailes.php">Pool View 2 Bedroom Apartment</a></h4>
								</div>
								<div class="proCon">Property Junction is Proud to Offer You for Sale This 2 Bedroom Apartment in Lake View  - JLT</div>
								<div class="proFooter">
									<div class="area"><strong>Area:</strong> 1500</div>
									<div class="fatr">
										<ul>
											<li>
												<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
													<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
													<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
													<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
												</svg>
												<span>2</span>
											</li>
											<li>
												<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
													<g>
														<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
													</g>
												</svg>
												<span>3</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="featuredPropertyWrapper">
							<div class="proImag">
								<figure style="background-image: url(images/proImg3.jpg)"></figure>
							</div>
							<div class="proInner">
								<div class="proHead">
									<h4><a href="propertyDetailes.php">SPLENDID 4 Bedroom + Den Available</a></h4>
								</div>
								<div class="proCon">Property Junction is Proud to Offer You for Sale This 2 Bedroom Apartment in Lake View  - JLT</div>
								<div class="proFooter">
									<div class="area"><strong>Area:</strong> 1500</div>
									<div class="fatr">
										<ul>
											<li>
												<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
													<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
													<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
													<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
												</svg>
												<span>2</span>
											</li>
											<li>
												<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
													<g>
														<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
													</g>
												</svg>
												<span>3</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="featuredPropertyWrapper">
							<div class="proImag">
								<figure style="background-image: url(images/proImg4.jpg)"></figure>
							</div>
							<div class="proInner">
								<div class="proHead">
									<h4><a href="propertyDetailes.php">Vacant Full Lake View Upgraded Unit</a></h4>
								</div>
								<div class="proCon">Property Junction is Proud to Offer You for Sale This 2 Bedroom Apartment in Lake View  - JLT</div>
								<div class="proFooter">
									<div class="area"><strong>Area:</strong> 1500</div>
									<div class="fatr">
										<ul>
											<li>
												<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
													<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
													<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
													<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
												</svg>
												<span>2</span>
											</li>
											<li>
												<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
													<g>
														<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
													</g>
												</svg>
												<span>3</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="featuredPropertyWrapper">
							<div class="proImag">
								<figure style="background-image: url(images/proImg1.jpg)"></figure>
							</div>
							<div class="proInner">
								<div class="proHead">
									<h4><a href="propertyDetailes.php">Vacant Full Lake View Upgraded Unit</a></h4>
								</div>
								<div class="proCon">Property Junction is Proud to Offer You for Sale This 2 Bedroom Apartment in Lake View  - JLT</div>
								<div class="proFooter">
									<div class="area"><strong>Area:</strong> 1500</div>
									<div class="fatr">
										<ul>
											<li>
												<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
													<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
													<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
													<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
												</svg>
												<span>2</span>
											</li>
											<li>
												<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
													<g>
														<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
													</g>
												</svg>
												<span>3</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="featuredPropertyWrapper">
							<div class="proImag">
								<figure style="background-image: url(images/proImg5.jpg)"></figure>
							</div>
							<div class="proInner">
								<div class="proHead">
									<h4><a href="propertyDetailes.php">Vacant Full Lake View Upgraded Unit</a></h4>
								</div>
								<div class="proCon">Property Junction is Proud to Offer You for Sale This 2 Bedroom Apartment in Lake View  - JLT</div>
								<div class="proFooter">
									<div class="area"><strong>Area:</strong> 1500</div>
									<div class="fatr">
										<ul>
											<li>
												<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
													<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
													<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
													<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
												</svg>
												<span>2</span>
											</li>
											<li>
												<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
													<g>
														<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
													</g>
												</svg>
												<span>3</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="featuredPropertyWrapper">
							<div class="proImag">
								<figure style="background-image: url(images/proImg6.jpg)"></figure>
							</div>
							<div class="proInner">
								<div class="proHead">
									<h4><a href="propertyDetailes.php">Vacant Full Lake View Upgraded Unit</a></h4>
								</div>
								<div class="proCon">Property Junction is Proud to Offer You for Sale This 2 Bedroom Apartment in Lake View  - JLT</div>
								<div class="proFooter">
									<div class="area"><strong>Area:</strong> 1500</div>
									<div class="fatr">
										<ul>
											<li>
												<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
													<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
													<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
													<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
												</svg>
												<span>2</span>
											</li>
											<li>
												<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
													<g>
														<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
													</g>
												</svg>
												<span>3</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="featuredPropertyWrapper">
							<div class="proImag">
								<figure style="background-image: url(images/proImg7.jpg)"></figure>
							</div>
							<div class="proInner">
								<div class="proHead">
									<h4><a href="propertyDetailes.php">Vacant Full Lake View Upgraded Unit</a></h4>
								</div>
								<div class="proCon">Property Junction is Proud to Offer You for Sale This 2 Bedroom Apartment in Lake View  - JLT</div>
								<div class="proFooter">
									<div class="area"><strong>Area:</strong> 1500</div>
									<div class="fatr">
										<ul>
											<li>
												<svg class="bath" xmlns="http://www.w3.org/2000/svg" viewBox="4.34 1.24 22.32 22.32">
													<path d="M5.58,3.1c0-0.342,0.278-0.62,0.62-0.62c0.634,0,1.23,0.247,1.678,0.695l0.399,0.399C7.74,4.3,7.44,5.18,7.44,6.094V6.2 l1.24,1.24l4.34-4.34l-1.24-1.24h-0.106c-0.914,0-1.794,0.3-2.52,0.837L8.755,2.298C8.073,1.616,7.165,1.24,6.2,1.24 c-1.026,0-1.86,0.834-1.86,1.86v8.06v4.873c0,0.469,0.065,0.936,0.194,1.387c0.32,1.121,1.006,2.059,1.904,2.705L5.58,23.561h2.48 l2.48-2.48h9.92l2.479,2.48h2.48l-0.858-3.436c0.897-0.646,1.584-1.585,1.904-2.705c0.129-0.451,0.194-0.918,0.194-1.387V11.16H5.58 V3.1z M23.436,20.735l0.396,1.585h-0.379l-1.275-1.275C22.615,20.996,23.037,20.891,23.436,20.735z M7.564,20.735 c0.399,0.155,0.821,0.261,1.257,0.31L7.546,22.32H7.168L7.564,20.735z"></path>
													<rect x="11.16" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="9.92" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="11.16" y="8.06" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="4.34" width="1.24" height="1.24"></rect>
													<rect x="13.64" y="5.58" width="1.24" height="1.24"></rect>
													<rect x="14.88" y="6.82" width="1.24" height="1.24"></rect>
													<rect x="16.12" y="8.06" width="1.239" height="1.24"></rect>
													<rect x="14.88" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="12.4" y="9.3" width="1.24" height="1.24"></rect>
													<rect x="17.359" y="9.3" width="1.24" height="1.24"></rect>
												</svg>
												<span>2</span>
											</li>
											<li>
												<svg class="bed" xmlns="http://www.w3.org/2000/svg" viewBox="5.39 2.125 22.221 18.575">
													<g>
														<path d="M25.131,7.928V2.869c0-0.421-0.322-0.744-0.744-0.744H8.614c-0.422,0-0.744,0.322-0.744,0.744v5.059l-2.431,5.804 C5.415,13.831,5.39,13.93,5.39,14.029v6.647h1.488v-2.158h19.246V20.7h1.487v-6.671c0-0.099-0.024-0.198-0.05-0.298L25.131,7.928z M18.707,6.886h2.753v2.282h-2.753V6.886z M11.54,6.886h2.753v2.282H11.54V6.886z M7.25,13.285l1.86-4.464h0.942v1.091 c0,0.421,0.323,0.744,0.744,0.744h4.241c0.422,0,0.744-0.322,0.744-0.744V8.821h1.438v1.091c0,0.421,0.322,0.744,0.744,0.744h4.241 c0.421,0,0.744-0.322,0.744-0.744V8.821h0.942l1.859,4.464H7.25z"></path>
													</g>
												</svg>
												<span>3</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd AgentsList overlayPattern" style="background-image: url(images/OurAgentsBg.jpg);">
		<div class="container">
			<h2 class="title">Our Agents <span>Listing agents list client homes and negotiate the best possible price</span></h2>

			<div id="agentSlider" class="owl-carousel owl-theme">
				<div class="item">
					<div class="agent">
						<div class="agentImg">
							<figure style="background-image: url(images/agent1.jpg);"></figure>
						</div>
						<div class="agentName">
							<h4>Mukul Wadhwa</h4>
							<p>Property Junction Real Estate</p>
						</div>
						<div class="userInfo">
							<div class="personal">
								<ul>
									<li><i class="fa fa-phone"></i><a href="tel:+971504437125">+971 504437125</a></li>
									<li><i class="fa fa-envelope-o"></i><a href="mailto:amish@propertyjunction.ae">amish@propertyjunction.ae</a></li>
								</ul>
							</div>
							<div class="social">
								<ul>
									<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="agent">
						<div class="agentImg">
							<figure style="background-image: url(images/agent2.jpg);"></figure>
						</div>
						<div class="agentName">
							<h4>Mukul Wadhwa</h4>
							<p>Property Junction Real Estate</p>
						</div>
						<div class="userInfo">
							<div class="personal">
								<ul>
									<li><i class="fa fa-phone"></i><a href="tel:+971504437125">+971 504437125</a></li>
									<li><i class="fa fa-envelope-o"></i><a href="mailto:amish@propertyjunction.ae">amish@propertyjunction.ae</a></li>
								</ul>
							</div>
							<div class="social">
								<ul>
									<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="agent">
						<div class="agentImg">
							<figure style="background-image: url(images/agent3.jpg);"></figure>
						</div>
						<div class="agentName">
							<h4>Admin Manager</h4>
							<p>Managing Director - Property Junction</p>
						</div>
						<div class="userInfo">
							<div class="personal">
								<ul>
									<li><i class="fa fa-phone"></i><a href="tel:+971504437125">+971 504437125</a></li>
									<li><i class="fa fa-envelope-o"></i><a href="mailto:amish@propertyjunction.ae">amish@propertyjunction.ae</a></li>
								</ul>
							</div>
							<div class="social">
								<ul>
									<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="agent">
						<div class="agentImg">
							<figure style="background-image: url(images/agent4.jpg);"></figure>
						</div>
						<div class="agentName">
							<h4>Aiman Zhumadilova</h4>
							<p>Managing Director - Property Junction Real Estate</p>
						</div>
						<div class="userInfo">
							<div class="personal">
								<ul>
									<li><i class="fa fa-phone"></i><a href="tel:+971504437125">+971 504437125</a></li>
									<li><i class="fa fa-envelope-o"></i><a href="mailto:amish@propertyjunction.ae">amish@propertyjunction.ae</a></li>
								</ul>
							</div>
							<div class="social">
								<ul>
									<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="agent">
						<div class="agentImg">
							<figure style="background-image: url(images/agent1.jpg);"></figure>
						</div>
						<div class="agentName">
							<h4>Mukul Wadhwa</h4>
							<p>Property Junction Real Estate</p>
						</div>
						<div class="userInfo">
							<div class="personal">
								<ul>
									<li><i class="fa fa-phone"></i><a href="tel:+971504437125">+971 504437125</a></li>
									<li><i class="fa fa-envelope-o"></i><a href="mailto:amish@propertyjunction.ae">amish@propertyjunction.ae</a></li>
								</ul>
							</div>
							<div class="social">
								<ul>
									<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="agent">
						<div class="agentImg">
							<figure style="background-image: url(images/agent2.jpg);"></figure>
						</div>
						<div class="agentName">
							<h4>Mukul Wadhwa</h4>
							<p>Property Junction Real Estate</p>
						</div>
						<div class="userInfo">
							<div class="personal">
								<ul>
									<li><i class="fa fa-phone"></i><a href="tel:+971504437125">+971 504437125</a></li>
									<li><i class="fa fa-envelope-o"></i><a href="mailto:amish@propertyjunction.ae">amish@propertyjunction.ae</a></li>
								</ul>
							</div>
							<div class="social">
								<ul>
									<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="agent">
						<div class="agentImg">
							<figure style="background-image: url(images/agent3.jpg);"></figure>
						</div>
						<div class="agentName">
							<h4>Admin Manager</h4>
							<p>Managing Director - Property Junction</p>
						</div>
						<div class="userInfo">
							<div class="personal">
								<ul>
									<li><i class="fa fa-phone"></i><a href="tel:+971504437125">+971 504437125</a></li>
									<li><i class="fa fa-envelope-o"></i><a href="mailto:amish@propertyjunction.ae">amish@propertyjunction.ae</a></li>
								</ul>
							</div>
							<div class="social">
								<ul>
									<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="agent">
						<div class="agentImg">
							<figure style="background-image: url(images/agent4.jpg);"></figure>
						</div>
						<div class="agentName">
							<h4>Aiman Zhumadilova</h4>
							<p>Managing Director - Property Junction Real Estate</p>
						</div>
						<div class="userInfo">
							<div class="personal">
								<ul>
									<li><i class="fa fa-phone"></i><a href="tel:+971504437125">+971 504437125</a></li>
									<li><i class="fa fa-envelope-o"></i><a href="mailto:amish@propertyjunction.ae">amish@propertyjunction.ae</a></li>
								</ul>
							</div>
							<div class="social">
								<ul>
									<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd">
		<div class="container">
			<h2 class="title">Latest Properties <span>Listing agents list client homes and negotiate the best possible price</span></h2>

			<div id="latestSlider" class="owl-carousel owl-theme">
				<div class="item">
					<div class="latestPorpWrapper">
						<div class="latestPorpImage">
							<figure style="background-image: url(images/latestPorp1.jpg);">
								<span>
									<span>$556.885</span>
									<span>$3.500/sq ft</span>
								</span>
							</figure>
						</div>
						<div class="latestPorpCon">
							<div class="latestPorpConWrapper">
								<h4>Guaranteed Modern Home</h4>
								<span>
									<svg class="pin" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 188.171 305.961">
										<g>
											<g>
												<path d="M188.024,93.942L188.024,93.942C188.024,42.067,145.957,0,94.082,0S0.15,42.067,0.15,93.942 c0,0-8.055,99.292,93.932,212.019C195.887,193.438,188.045,94.307,188.024,93.942z M94.082,160.326 c-35.279,0-63.881-28.612-63.881-63.871c0-35.299,28.602-63.871,63.881-63.871c35.279,0,63.871,28.572,63.871,63.871 C157.953,131.714,129.361,160.326,94.082,160.326z"></path>
												<path d="M94.082,49.038c24.965,0,45.188,20.223,45.188,45.168c0,24.944-20.223,45.188-45.188,45.188 c-24.955,0-45.178-20.243-45.178-45.188C48.904,69.261,69.127,49.038,94.082,49.038z"></path>
											</g>
										</g>
									</svg>
									<small>Louis, Missouri, us</small>
								</span>
								<p>Bedroom: 4 Bathroom:2 Sq Ft:750 <br>Apartment</p>
							</div>
							<div class="latestPorpFooter">
								<div class="owner">
									<svg class="owner" xmlns="http://www.w3.org/2000/svg" viewBox="5.828 1.364 19.344 22.072">
										<g transform="translate(0,-952.36218)">
											<path d="M18.738,953.753c-0.402,0.202-1.44,0.347-2.363,0.411c-0.638,0.048-1.274,0.06-1.875,0.078 c-1.521-0.002-2.528,0.607-3.099,1.596c-0.545,0.944-0.72,2.204-0.736,3.619c-0.659-0.134-0.729,0.514-0.729,1.007 c0.108,0.702,0.589,1.637,1.178,1.364c0.253,0.835,0.597,1.702,1.022,2.495c0.336,0.626,0.72,1.199,1.155,1.666 c-0.168,0.452-0.314,1.035-0.457,1.534c-1.806,0.938-3.684,1.248-5.408,2.363c-0.499,0.34-1.077,1.093-1.093,2.154 c-0.178,1.141-0.338,2.317-0.503,3.472c-0.022,0.142,0.104,0.288,0.248,0.286h18.843c0.144,0.003,0.27-0.145,0.248-0.286 c-0.175-1.153-0.316-2.333-0.504-3.472c-0.014-1.054-0.518-1.867-1.123-2.17c-0.484-0.242-3.211-1.552-5.385-2.363l-0.396-1.387 c0.502-0.483,0.922-1.103,1.264-1.774c0.398-0.784,0.705-1.648,0.922-2.479c0.688,0.021,1.033-0.861,1.146-1.403 c0.012-0.602-0.111-1.109-0.759-0.992c-0.017-1.704-0.07-2.839-0.248-3.681c-0.185-0.877-0.522-1.452-1.062-1.991 C18.896,953.676,18.854,953.744,18.738,953.753z M18.746,954.249c0.432,0.455,0.693,0.898,0.853,1.65 c0.135,0.639,0.197,1.521,0.225,2.759c-0.266-0.816-0.884-1.588-1.619-1.875c-0.547-0.205-1.067-0.237-1.434-0.054 c-0.774,0.222-1.801,0.196-2.51,0c-0.484-0.19-0.997-0.107-1.433,0.054c-0.928,0.458-1.396,1.129-1.635,1.929 c0.066-1.056,0.252-1.963,0.635-2.626c0.496-0.859,1.287-1.356,2.696-1.349c0.637-0.014,1.323-0.039,1.882-0.077 C17.262,954.6,18.143,954.486,18.746,954.249z M14.043,957.17c0.914,0.246,1.953,0.276,2.945,0c0.129-0.065,0.6-0.089,1.045,0.077 c0.791,0.407,1.146,0.983,1.333,1.627c0.511,1.818,0.067,3.546-0.782,5.246c-0.732,1.438-1.756,2.502-3.115,2.502 c-1.436-0.153-2.312-1.461-2.897-2.542c-0.932-1.778-1.457-3.236-0.907-5.207c0.194-0.714,0.706-1.383,1.333-1.627 C13.366,957.12,13.671,957.099,14.043,957.17z M13.687,966.368c0.528,0.453,1.129,0.751,1.782,0.751 c0.71,0,1.333-0.242,1.875-0.643l0.264,0.922c-0.75,1.049-1.475,2.222-2.107,3.2c-0.76-1.008-1.293-2.253-2.107-3.2L13.687,966.368 z M13.176,967.926l1.929,2.96c-0.242,0.307-0.394,0.719-0.573,1.076c-0.034,0.229,0.057,0.29,0.209,0.442l-0.31,1.696l-2.363-5.71 C12.455,968.23,12.842,968.071,13.176,967.926z M17.832,967.926c0.356,0.13,0.734,0.28,1.115,0.434l-2.378,5.741l-0.31-1.696 c0.18-0.128,0.202-0.276,0.209-0.442c-0.181-0.352-0.354-0.743-0.558-1.068C16.586,969.859,17.119,968.97,17.832,967.926z M19.412,968.546c0.451,0.187,0.891,0.377,1.318,0.565l-1.721,2.58c-0.062,0.094-0.048,0.229,0.031,0.31l0.828,0.83l-2.254,2.471 h-1L19.412,968.546z M11.603,968.584l2.781,6.718h-1l-2.254-2.471l0.829-0.83c0.079-0.08,0.093-0.215,0.031-0.31l-1.712-2.565 C10.716,968.946,11.166,968.763,11.603,968.584L11.603,968.584z M9.813,969.32l1.65,2.472l-0.852,0.853 c-0.088,0.089-0.092,0.248-0.008,0.341l2.107,2.316H6.365l0.457-3.191c0.055-0.665,0.349-1.434,0.883-1.813 C7.988,970.108,8.836,969.729,9.813,969.32L9.813,969.32z M21.188,969.32c1.055,0.473,1.902,0.874,2.138,0.991 c0.378,0.189,0.853,0.858,0.853,1.768c0.144,1.072,0.303,2.15,0.457,3.223h-6.346l2.107-2.316c0.084-0.093,0.08-0.252-0.008-0.341 l-0.852-0.853L21.188,969.32z M15.407,971.335h0.186l0.349,0.697c-0.136,0.087-0.159,0.213-0.186,0.334l0.465,2.572l-0.147,0.363 h-1.146l-0.147-0.363l0.465-2.572c0-0.172-0.071-0.224-0.186-0.334L15.407,971.335z"></path>
										</g>
									</svg>
									<span>Maria Barlow</span>
								</div>
								<div class="postTime">
									<svg class="calender" xmlns="http://www.w3.org/2000/svg" width="26.618px" height="24.8px" viewBox="2.191 0 26.618 24.8" enable-background="new 2.191 0 26.618 24.8" xml:space="preserve">
										<g transform="translate(-94.922001,-429.7886)">
											<path d="M103.685,429.789c-0.771,0-1.395,0.623-1.395,1.391v4.6c0,0.768,0.626,1.391,1.395,1.391h1.61 c0.771,0,1.395-0.623,1.395-1.391v-4.6c0-0.77-0.625-1.391-1.395-1.391H103.685z M115.926,429.789c-0.77,0-1.391,0.623-1.391,1.391 v4.6c0,0.768,0.623,1.391,1.391,1.391h1.611c0.771,0,1.395-0.623,1.395-1.391v-4.6c0-0.77-0.627-1.391-1.395-1.391H115.926z M100.285,433.481c-1.755,0-3.172,1.419-3.172,3.172v14.758c0,1.746,1.417,3.177,3.172,3.177h20.27 c1.747,0,3.176-1.431,3.176-3.177v-14.758c0-1.755-1.429-3.172-3.176-3.172h-0.707v2.293c0,1.272-1.039,2.31-2.311,2.31h-1.606 c-1.272,0-2.302-1.038-2.302-2.31v-2.293h-6.032v2.293c0,1.272-1.029,2.31-2.302,2.31h-1.606c-1.272,0-2.302-1.038-2.302-2.31 v-2.293H100.285z M99.94,439.728h20.961v9.907c0,1.188-0.963,2.151-2.151,2.151h-16.658c-1.189,0-2.151-0.963-2.151-2.151V439.728z M101.839,440.977v1.659h-1.005v0.911h1.005v1.708h-1.005v0.911h1.005v1.705h-1.005v0.91h1.005v1.461h0.911v-1.461h3.25v1.461 h0.911v-1.461h3.25v1.461h0.911v-1.461h3.25v1.461h0.91v-1.461h3.254v1.461h0.911v-1.461h1.001v-0.91h-1.001v-1.705h1.001v-0.911 h-1.001v-1.708h1.001v-0.911h-1.001v-1.659h-0.911v1.659h-3.254v-1.659h-0.91v1.659h-3.25v-1.659h-0.911v1.659h-3.25v-1.659h-0.911 v1.659h-3.25v-1.659H101.839L101.839,440.977z M102.75,443.547h3.25v1.708h-3.25V443.547z M106.906,443.547h3.253v1.708h-3.253 V443.547z M111.067,443.547h3.25v1.708h-3.25V443.547z M115.227,443.547h3.254v1.708h-3.254V443.547z M102.75,446.166h3.25v1.705 h-3.25V446.166z M106.906,446.166h3.253v1.705h-3.253V446.166z M111.067,446.166h3.25v1.705h-3.25V446.166z M115.227,446.166h3.254 v1.705h-3.254V446.166z"></path>
										</g>
									</svg>
									<span>10 Days ago</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="latestPorpWrapper">
						<div class="latestPorpImage">
							<figure style="background-image: url(images/latestPorp2.jpg);">
								<span>
									<span>$556.885</span>
									<span>$3.500/sq ft</span>
								</span>
							</figure>
						</div>
						<div class="latestPorpCon">
							<div class="latestPorpConWrapper">
								<h4>Guaranteed Modern Home</h4>
								<span>
									<svg class="pin" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 188.171 305.961">
										<g>
											<g>
												<path d="M188.024,93.942L188.024,93.942C188.024,42.067,145.957,0,94.082,0S0.15,42.067,0.15,93.942 c0,0-8.055,99.292,93.932,212.019C195.887,193.438,188.045,94.307,188.024,93.942z M94.082,160.326 c-35.279,0-63.881-28.612-63.881-63.871c0-35.299,28.602-63.871,63.881-63.871c35.279,0,63.871,28.572,63.871,63.871 C157.953,131.714,129.361,160.326,94.082,160.326z"></path>
												<path d="M94.082,49.038c24.965,0,45.188,20.223,45.188,45.168c0,24.944-20.223,45.188-45.188,45.188 c-24.955,0-45.178-20.243-45.178-45.188C48.904,69.261,69.127,49.038,94.082,49.038z"></path>
											</g>
										</g>
									</svg>
									<small>Louis, Missouri, us</small>
								</span>
								<p>Bedroom: 4 Bathroom:2 Sq Ft:750 <br>Apartment</p>
							</div>
							<div class="latestPorpFooter">
								<div class="owner">
									<svg class="owner" xmlns="http://www.w3.org/2000/svg" viewBox="5.828 1.364 19.344 22.072">
										<g transform="translate(0,-952.36218)">
											<path d="M18.738,953.753c-0.402,0.202-1.44,0.347-2.363,0.411c-0.638,0.048-1.274,0.06-1.875,0.078 c-1.521-0.002-2.528,0.607-3.099,1.596c-0.545,0.944-0.72,2.204-0.736,3.619c-0.659-0.134-0.729,0.514-0.729,1.007 c0.108,0.702,0.589,1.637,1.178,1.364c0.253,0.835,0.597,1.702,1.022,2.495c0.336,0.626,0.72,1.199,1.155,1.666 c-0.168,0.452-0.314,1.035-0.457,1.534c-1.806,0.938-3.684,1.248-5.408,2.363c-0.499,0.34-1.077,1.093-1.093,2.154 c-0.178,1.141-0.338,2.317-0.503,3.472c-0.022,0.142,0.104,0.288,0.248,0.286h18.843c0.144,0.003,0.27-0.145,0.248-0.286 c-0.175-1.153-0.316-2.333-0.504-3.472c-0.014-1.054-0.518-1.867-1.123-2.17c-0.484-0.242-3.211-1.552-5.385-2.363l-0.396-1.387 c0.502-0.483,0.922-1.103,1.264-1.774c0.398-0.784,0.705-1.648,0.922-2.479c0.688,0.021,1.033-0.861,1.146-1.403 c0.012-0.602-0.111-1.109-0.759-0.992c-0.017-1.704-0.07-2.839-0.248-3.681c-0.185-0.877-0.522-1.452-1.062-1.991 C18.896,953.676,18.854,953.744,18.738,953.753z M18.746,954.249c0.432,0.455,0.693,0.898,0.853,1.65 c0.135,0.639,0.197,1.521,0.225,2.759c-0.266-0.816-0.884-1.588-1.619-1.875c-0.547-0.205-1.067-0.237-1.434-0.054 c-0.774,0.222-1.801,0.196-2.51,0c-0.484-0.19-0.997-0.107-1.433,0.054c-0.928,0.458-1.396,1.129-1.635,1.929 c0.066-1.056,0.252-1.963,0.635-2.626c0.496-0.859,1.287-1.356,2.696-1.349c0.637-0.014,1.323-0.039,1.882-0.077 C17.262,954.6,18.143,954.486,18.746,954.249z M14.043,957.17c0.914,0.246,1.953,0.276,2.945,0c0.129-0.065,0.6-0.089,1.045,0.077 c0.791,0.407,1.146,0.983,1.333,1.627c0.511,1.818,0.067,3.546-0.782,5.246c-0.732,1.438-1.756,2.502-3.115,2.502 c-1.436-0.153-2.312-1.461-2.897-2.542c-0.932-1.778-1.457-3.236-0.907-5.207c0.194-0.714,0.706-1.383,1.333-1.627 C13.366,957.12,13.671,957.099,14.043,957.17z M13.687,966.368c0.528,0.453,1.129,0.751,1.782,0.751 c0.71,0,1.333-0.242,1.875-0.643l0.264,0.922c-0.75,1.049-1.475,2.222-2.107,3.2c-0.76-1.008-1.293-2.253-2.107-3.2L13.687,966.368 z M13.176,967.926l1.929,2.96c-0.242,0.307-0.394,0.719-0.573,1.076c-0.034,0.229,0.057,0.29,0.209,0.442l-0.31,1.696l-2.363-5.71 C12.455,968.23,12.842,968.071,13.176,967.926z M17.832,967.926c0.356,0.13,0.734,0.28,1.115,0.434l-2.378,5.741l-0.31-1.696 c0.18-0.128,0.202-0.276,0.209-0.442c-0.181-0.352-0.354-0.743-0.558-1.068C16.586,969.859,17.119,968.97,17.832,967.926z M19.412,968.546c0.451,0.187,0.891,0.377,1.318,0.565l-1.721,2.58c-0.062,0.094-0.048,0.229,0.031,0.31l0.828,0.83l-2.254,2.471 h-1L19.412,968.546z M11.603,968.584l2.781,6.718h-1l-2.254-2.471l0.829-0.83c0.079-0.08,0.093-0.215,0.031-0.31l-1.712-2.565 C10.716,968.946,11.166,968.763,11.603,968.584L11.603,968.584z M9.813,969.32l1.65,2.472l-0.852,0.853 c-0.088,0.089-0.092,0.248-0.008,0.341l2.107,2.316H6.365l0.457-3.191c0.055-0.665,0.349-1.434,0.883-1.813 C7.988,970.108,8.836,969.729,9.813,969.32L9.813,969.32z M21.188,969.32c1.055,0.473,1.902,0.874,2.138,0.991 c0.378,0.189,0.853,0.858,0.853,1.768c0.144,1.072,0.303,2.15,0.457,3.223h-6.346l2.107-2.316c0.084-0.093,0.08-0.252-0.008-0.341 l-0.852-0.853L21.188,969.32z M15.407,971.335h0.186l0.349,0.697c-0.136,0.087-0.159,0.213-0.186,0.334l0.465,2.572l-0.147,0.363 h-1.146l-0.147-0.363l0.465-2.572c0-0.172-0.071-0.224-0.186-0.334L15.407,971.335z"></path>
										</g>
									</svg>
									<span>Maria Barlow</span>
								</div>
								<div class="postTime">
									<svg class="calender" xmlns="http://www.w3.org/2000/svg" width="26.618px" height="24.8px" viewBox="2.191 0 26.618 24.8" enable-background="new 2.191 0 26.618 24.8" xml:space="preserve">
										<g transform="translate(-94.922001,-429.7886)">
											<path d="M103.685,429.789c-0.771,0-1.395,0.623-1.395,1.391v4.6c0,0.768,0.626,1.391,1.395,1.391h1.61 c0.771,0,1.395-0.623,1.395-1.391v-4.6c0-0.77-0.625-1.391-1.395-1.391H103.685z M115.926,429.789c-0.77,0-1.391,0.623-1.391,1.391 v4.6c0,0.768,0.623,1.391,1.391,1.391h1.611c0.771,0,1.395-0.623,1.395-1.391v-4.6c0-0.77-0.627-1.391-1.395-1.391H115.926z M100.285,433.481c-1.755,0-3.172,1.419-3.172,3.172v14.758c0,1.746,1.417,3.177,3.172,3.177h20.27 c1.747,0,3.176-1.431,3.176-3.177v-14.758c0-1.755-1.429-3.172-3.176-3.172h-0.707v2.293c0,1.272-1.039,2.31-2.311,2.31h-1.606 c-1.272,0-2.302-1.038-2.302-2.31v-2.293h-6.032v2.293c0,1.272-1.029,2.31-2.302,2.31h-1.606c-1.272,0-2.302-1.038-2.302-2.31 v-2.293H100.285z M99.94,439.728h20.961v9.907c0,1.188-0.963,2.151-2.151,2.151h-16.658c-1.189,0-2.151-0.963-2.151-2.151V439.728z M101.839,440.977v1.659h-1.005v0.911h1.005v1.708h-1.005v0.911h1.005v1.705h-1.005v0.91h1.005v1.461h0.911v-1.461h3.25v1.461 h0.911v-1.461h3.25v1.461h0.911v-1.461h3.25v1.461h0.91v-1.461h3.254v1.461h0.911v-1.461h1.001v-0.91h-1.001v-1.705h1.001v-0.911 h-1.001v-1.708h1.001v-0.911h-1.001v-1.659h-0.911v1.659h-3.254v-1.659h-0.91v1.659h-3.25v-1.659h-0.911v1.659h-3.25v-1.659h-0.911 v1.659h-3.25v-1.659H101.839L101.839,440.977z M102.75,443.547h3.25v1.708h-3.25V443.547z M106.906,443.547h3.253v1.708h-3.253 V443.547z M111.067,443.547h3.25v1.708h-3.25V443.547z M115.227,443.547h3.254v1.708h-3.254V443.547z M102.75,446.166h3.25v1.705 h-3.25V446.166z M106.906,446.166h3.253v1.705h-3.253V446.166z M111.067,446.166h3.25v1.705h-3.25V446.166z M115.227,446.166h3.254 v1.705h-3.254V446.166z"></path>
										</g>
									</svg>
									<span>10 Days ago</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="latestPorpWrapper">
						<div class="latestPorpImage">
							<figure style="background-image: url(images/latestPorp3.jpg);">
								<span>
									<span>$556.885</span>
									<span>$3.500/sq ft</span>
								</span>
							</figure>
						</div>
						<div class="latestPorpCon">
							<div class="latestPorpConWrapper">
								<h4>Guaranteed Modern Home</h4>
								<span>
									<svg class="pin" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 188.171 305.961">
										<g>
											<g>
												<path d="M188.024,93.942L188.024,93.942C188.024,42.067,145.957,0,94.082,0S0.15,42.067,0.15,93.942 c0,0-8.055,99.292,93.932,212.019C195.887,193.438,188.045,94.307,188.024,93.942z M94.082,160.326 c-35.279,0-63.881-28.612-63.881-63.871c0-35.299,28.602-63.871,63.881-63.871c35.279,0,63.871,28.572,63.871,63.871 C157.953,131.714,129.361,160.326,94.082,160.326z"></path>
												<path d="M94.082,49.038c24.965,0,45.188,20.223,45.188,45.168c0,24.944-20.223,45.188-45.188,45.188 c-24.955,0-45.178-20.243-45.178-45.188C48.904,69.261,69.127,49.038,94.082,49.038z"></path>
											</g>
										</g>
									</svg>
									<small>Louis, Missouri, us</small>
								</span>
								<p>Bedroom: 4 Bathroom:2 Sq Ft:750 <br>Apartment</p>
							</div>
							<div class="latestPorpFooter">
								<div class="owner">
									<svg class="owner" xmlns="http://www.w3.org/2000/svg" viewBox="5.828 1.364 19.344 22.072">
										<g transform="translate(0,-952.36218)">
											<path d="M18.738,953.753c-0.402,0.202-1.44,0.347-2.363,0.411c-0.638,0.048-1.274,0.06-1.875,0.078 c-1.521-0.002-2.528,0.607-3.099,1.596c-0.545,0.944-0.72,2.204-0.736,3.619c-0.659-0.134-0.729,0.514-0.729,1.007 c0.108,0.702,0.589,1.637,1.178,1.364c0.253,0.835,0.597,1.702,1.022,2.495c0.336,0.626,0.72,1.199,1.155,1.666 c-0.168,0.452-0.314,1.035-0.457,1.534c-1.806,0.938-3.684,1.248-5.408,2.363c-0.499,0.34-1.077,1.093-1.093,2.154 c-0.178,1.141-0.338,2.317-0.503,3.472c-0.022,0.142,0.104,0.288,0.248,0.286h18.843c0.144,0.003,0.27-0.145,0.248-0.286 c-0.175-1.153-0.316-2.333-0.504-3.472c-0.014-1.054-0.518-1.867-1.123-2.17c-0.484-0.242-3.211-1.552-5.385-2.363l-0.396-1.387 c0.502-0.483,0.922-1.103,1.264-1.774c0.398-0.784,0.705-1.648,0.922-2.479c0.688,0.021,1.033-0.861,1.146-1.403 c0.012-0.602-0.111-1.109-0.759-0.992c-0.017-1.704-0.07-2.839-0.248-3.681c-0.185-0.877-0.522-1.452-1.062-1.991 C18.896,953.676,18.854,953.744,18.738,953.753z M18.746,954.249c0.432,0.455,0.693,0.898,0.853,1.65 c0.135,0.639,0.197,1.521,0.225,2.759c-0.266-0.816-0.884-1.588-1.619-1.875c-0.547-0.205-1.067-0.237-1.434-0.054 c-0.774,0.222-1.801,0.196-2.51,0c-0.484-0.19-0.997-0.107-1.433,0.054c-0.928,0.458-1.396,1.129-1.635,1.929 c0.066-1.056,0.252-1.963,0.635-2.626c0.496-0.859,1.287-1.356,2.696-1.349c0.637-0.014,1.323-0.039,1.882-0.077 C17.262,954.6,18.143,954.486,18.746,954.249z M14.043,957.17c0.914,0.246,1.953,0.276,2.945,0c0.129-0.065,0.6-0.089,1.045,0.077 c0.791,0.407,1.146,0.983,1.333,1.627c0.511,1.818,0.067,3.546-0.782,5.246c-0.732,1.438-1.756,2.502-3.115,2.502 c-1.436-0.153-2.312-1.461-2.897-2.542c-0.932-1.778-1.457-3.236-0.907-5.207c0.194-0.714,0.706-1.383,1.333-1.627 C13.366,957.12,13.671,957.099,14.043,957.17z M13.687,966.368c0.528,0.453,1.129,0.751,1.782,0.751 c0.71,0,1.333-0.242,1.875-0.643l0.264,0.922c-0.75,1.049-1.475,2.222-2.107,3.2c-0.76-1.008-1.293-2.253-2.107-3.2L13.687,966.368 z M13.176,967.926l1.929,2.96c-0.242,0.307-0.394,0.719-0.573,1.076c-0.034,0.229,0.057,0.29,0.209,0.442l-0.31,1.696l-2.363-5.71 C12.455,968.23,12.842,968.071,13.176,967.926z M17.832,967.926c0.356,0.13,0.734,0.28,1.115,0.434l-2.378,5.741l-0.31-1.696 c0.18-0.128,0.202-0.276,0.209-0.442c-0.181-0.352-0.354-0.743-0.558-1.068C16.586,969.859,17.119,968.97,17.832,967.926z M19.412,968.546c0.451,0.187,0.891,0.377,1.318,0.565l-1.721,2.58c-0.062,0.094-0.048,0.229,0.031,0.31l0.828,0.83l-2.254,2.471 h-1L19.412,968.546z M11.603,968.584l2.781,6.718h-1l-2.254-2.471l0.829-0.83c0.079-0.08,0.093-0.215,0.031-0.31l-1.712-2.565 C10.716,968.946,11.166,968.763,11.603,968.584L11.603,968.584z M9.813,969.32l1.65,2.472l-0.852,0.853 c-0.088,0.089-0.092,0.248-0.008,0.341l2.107,2.316H6.365l0.457-3.191c0.055-0.665,0.349-1.434,0.883-1.813 C7.988,970.108,8.836,969.729,9.813,969.32L9.813,969.32z M21.188,969.32c1.055,0.473,1.902,0.874,2.138,0.991 c0.378,0.189,0.853,0.858,0.853,1.768c0.144,1.072,0.303,2.15,0.457,3.223h-6.346l2.107-2.316c0.084-0.093,0.08-0.252-0.008-0.341 l-0.852-0.853L21.188,969.32z M15.407,971.335h0.186l0.349,0.697c-0.136,0.087-0.159,0.213-0.186,0.334l0.465,2.572l-0.147,0.363 h-1.146l-0.147-0.363l0.465-2.572c0-0.172-0.071-0.224-0.186-0.334L15.407,971.335z"></path>
										</g>
									</svg>
									<span>Maria Barlow</span>
								</div>
								<div class="postTime">
									<svg class="calender" xmlns="http://www.w3.org/2000/svg" width="26.618px" height="24.8px" viewBox="2.191 0 26.618 24.8" enable-background="new 2.191 0 26.618 24.8" xml:space="preserve">
										<g transform="translate(-94.922001,-429.7886)">
											<path d="M103.685,429.789c-0.771,0-1.395,0.623-1.395,1.391v4.6c0,0.768,0.626,1.391,1.395,1.391h1.61 c0.771,0,1.395-0.623,1.395-1.391v-4.6c0-0.77-0.625-1.391-1.395-1.391H103.685z M115.926,429.789c-0.77,0-1.391,0.623-1.391,1.391 v4.6c0,0.768,0.623,1.391,1.391,1.391h1.611c0.771,0,1.395-0.623,1.395-1.391v-4.6c0-0.77-0.627-1.391-1.395-1.391H115.926z M100.285,433.481c-1.755,0-3.172,1.419-3.172,3.172v14.758c0,1.746,1.417,3.177,3.172,3.177h20.27 c1.747,0,3.176-1.431,3.176-3.177v-14.758c0-1.755-1.429-3.172-3.176-3.172h-0.707v2.293c0,1.272-1.039,2.31-2.311,2.31h-1.606 c-1.272,0-2.302-1.038-2.302-2.31v-2.293h-6.032v2.293c0,1.272-1.029,2.31-2.302,2.31h-1.606c-1.272,0-2.302-1.038-2.302-2.31 v-2.293H100.285z M99.94,439.728h20.961v9.907c0,1.188-0.963,2.151-2.151,2.151h-16.658c-1.189,0-2.151-0.963-2.151-2.151V439.728z M101.839,440.977v1.659h-1.005v0.911h1.005v1.708h-1.005v0.911h1.005v1.705h-1.005v0.91h1.005v1.461h0.911v-1.461h3.25v1.461 h0.911v-1.461h3.25v1.461h0.911v-1.461h3.25v1.461h0.91v-1.461h3.254v1.461h0.911v-1.461h1.001v-0.91h-1.001v-1.705h1.001v-0.911 h-1.001v-1.708h1.001v-0.911h-1.001v-1.659h-0.911v1.659h-3.254v-1.659h-0.91v1.659h-3.25v-1.659h-0.911v1.659h-3.25v-1.659h-0.911 v1.659h-3.25v-1.659H101.839L101.839,440.977z M102.75,443.547h3.25v1.708h-3.25V443.547z M106.906,443.547h3.253v1.708h-3.253 V443.547z M111.067,443.547h3.25v1.708h-3.25V443.547z M115.227,443.547h3.254v1.708h-3.254V443.547z M102.75,446.166h3.25v1.705 h-3.25V446.166z M106.906,446.166h3.253v1.705h-3.253V446.166z M111.067,446.166h3.25v1.705h-3.25V446.166z M115.227,446.166h3.254 v1.705h-3.254V446.166z"></path>
										</g>
									</svg>
									<span>10 Days ago</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="latestPorpWrapper">
						<div class="latestPorpImage">
							<figure style="background-image: url(images/latestPorp4.jpg);">
								<span>
									<span>$556.885</span>
									<span>$3.500/sq ft</span>
								</span>
							</figure>
						</div>
						<div class="latestPorpCon">
							<div class="latestPorpConWrapper">
								<h4>Guaranteed Modern Home</h4>
								<span>
									<svg class="pin" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 188.171 305.961">
										<g>
											<g>
												<path d="M188.024,93.942L188.024,93.942C188.024,42.067,145.957,0,94.082,0S0.15,42.067,0.15,93.942 c0,0-8.055,99.292,93.932,212.019C195.887,193.438,188.045,94.307,188.024,93.942z M94.082,160.326 c-35.279,0-63.881-28.612-63.881-63.871c0-35.299,28.602-63.871,63.881-63.871c35.279,0,63.871,28.572,63.871,63.871 C157.953,131.714,129.361,160.326,94.082,160.326z"></path>
												<path d="M94.082,49.038c24.965,0,45.188,20.223,45.188,45.168c0,24.944-20.223,45.188-45.188,45.188 c-24.955,0-45.178-20.243-45.178-45.188C48.904,69.261,69.127,49.038,94.082,49.038z"></path>
											</g>
										</g>
									</svg>
									<small>Louis, Missouri, us</small>
								</span>
								<p>Bedroom: 4 Bathroom:2 Sq Ft:750 <br>Apartment</p>
							</div>
							<div class="latestPorpFooter">
								<div class="owner">
									<svg class="owner" xmlns="http://www.w3.org/2000/svg" viewBox="5.828 1.364 19.344 22.072">
										<g transform="translate(0,-952.36218)">
											<path d="M18.738,953.753c-0.402,0.202-1.44,0.347-2.363,0.411c-0.638,0.048-1.274,0.06-1.875,0.078 c-1.521-0.002-2.528,0.607-3.099,1.596c-0.545,0.944-0.72,2.204-0.736,3.619c-0.659-0.134-0.729,0.514-0.729,1.007 c0.108,0.702,0.589,1.637,1.178,1.364c0.253,0.835,0.597,1.702,1.022,2.495c0.336,0.626,0.72,1.199,1.155,1.666 c-0.168,0.452-0.314,1.035-0.457,1.534c-1.806,0.938-3.684,1.248-5.408,2.363c-0.499,0.34-1.077,1.093-1.093,2.154 c-0.178,1.141-0.338,2.317-0.503,3.472c-0.022,0.142,0.104,0.288,0.248,0.286h18.843c0.144,0.003,0.27-0.145,0.248-0.286 c-0.175-1.153-0.316-2.333-0.504-3.472c-0.014-1.054-0.518-1.867-1.123-2.17c-0.484-0.242-3.211-1.552-5.385-2.363l-0.396-1.387 c0.502-0.483,0.922-1.103,1.264-1.774c0.398-0.784,0.705-1.648,0.922-2.479c0.688,0.021,1.033-0.861,1.146-1.403 c0.012-0.602-0.111-1.109-0.759-0.992c-0.017-1.704-0.07-2.839-0.248-3.681c-0.185-0.877-0.522-1.452-1.062-1.991 C18.896,953.676,18.854,953.744,18.738,953.753z M18.746,954.249c0.432,0.455,0.693,0.898,0.853,1.65 c0.135,0.639,0.197,1.521,0.225,2.759c-0.266-0.816-0.884-1.588-1.619-1.875c-0.547-0.205-1.067-0.237-1.434-0.054 c-0.774,0.222-1.801,0.196-2.51,0c-0.484-0.19-0.997-0.107-1.433,0.054c-0.928,0.458-1.396,1.129-1.635,1.929 c0.066-1.056,0.252-1.963,0.635-2.626c0.496-0.859,1.287-1.356,2.696-1.349c0.637-0.014,1.323-0.039,1.882-0.077 C17.262,954.6,18.143,954.486,18.746,954.249z M14.043,957.17c0.914,0.246,1.953,0.276,2.945,0c0.129-0.065,0.6-0.089,1.045,0.077 c0.791,0.407,1.146,0.983,1.333,1.627c0.511,1.818,0.067,3.546-0.782,5.246c-0.732,1.438-1.756,2.502-3.115,2.502 c-1.436-0.153-2.312-1.461-2.897-2.542c-0.932-1.778-1.457-3.236-0.907-5.207c0.194-0.714,0.706-1.383,1.333-1.627 C13.366,957.12,13.671,957.099,14.043,957.17z M13.687,966.368c0.528,0.453,1.129,0.751,1.782,0.751 c0.71,0,1.333-0.242,1.875-0.643l0.264,0.922c-0.75,1.049-1.475,2.222-2.107,3.2c-0.76-1.008-1.293-2.253-2.107-3.2L13.687,966.368 z M13.176,967.926l1.929,2.96c-0.242,0.307-0.394,0.719-0.573,1.076c-0.034,0.229,0.057,0.29,0.209,0.442l-0.31,1.696l-2.363-5.71 C12.455,968.23,12.842,968.071,13.176,967.926z M17.832,967.926c0.356,0.13,0.734,0.28,1.115,0.434l-2.378,5.741l-0.31-1.696 c0.18-0.128,0.202-0.276,0.209-0.442c-0.181-0.352-0.354-0.743-0.558-1.068C16.586,969.859,17.119,968.97,17.832,967.926z M19.412,968.546c0.451,0.187,0.891,0.377,1.318,0.565l-1.721,2.58c-0.062,0.094-0.048,0.229,0.031,0.31l0.828,0.83l-2.254,2.471 h-1L19.412,968.546z M11.603,968.584l2.781,6.718h-1l-2.254-2.471l0.829-0.83c0.079-0.08,0.093-0.215,0.031-0.31l-1.712-2.565 C10.716,968.946,11.166,968.763,11.603,968.584L11.603,968.584z M9.813,969.32l1.65,2.472l-0.852,0.853 c-0.088,0.089-0.092,0.248-0.008,0.341l2.107,2.316H6.365l0.457-3.191c0.055-0.665,0.349-1.434,0.883-1.813 C7.988,970.108,8.836,969.729,9.813,969.32L9.813,969.32z M21.188,969.32c1.055,0.473,1.902,0.874,2.138,0.991 c0.378,0.189,0.853,0.858,0.853,1.768c0.144,1.072,0.303,2.15,0.457,3.223h-6.346l2.107-2.316c0.084-0.093,0.08-0.252-0.008-0.341 l-0.852-0.853L21.188,969.32z M15.407,971.335h0.186l0.349,0.697c-0.136,0.087-0.159,0.213-0.186,0.334l0.465,2.572l-0.147,0.363 h-1.146l-0.147-0.363l0.465-2.572c0-0.172-0.071-0.224-0.186-0.334L15.407,971.335z"></path>
										</g>
									</svg>
									<span>Maria Barlow</span>
								</div>
								<div class="postTime">
									<svg class="calender" xmlns="http://www.w3.org/2000/svg" width="26.618px" height="24.8px" viewBox="2.191 0 26.618 24.8" enable-background="new 2.191 0 26.618 24.8" xml:space="preserve">
										<g transform="translate(-94.922001,-429.7886)">
											<path d="M103.685,429.789c-0.771,0-1.395,0.623-1.395,1.391v4.6c0,0.768,0.626,1.391,1.395,1.391h1.61 c0.771,0,1.395-0.623,1.395-1.391v-4.6c0-0.77-0.625-1.391-1.395-1.391H103.685z M115.926,429.789c-0.77,0-1.391,0.623-1.391,1.391 v4.6c0,0.768,0.623,1.391,1.391,1.391h1.611c0.771,0,1.395-0.623,1.395-1.391v-4.6c0-0.77-0.627-1.391-1.395-1.391H115.926z M100.285,433.481c-1.755,0-3.172,1.419-3.172,3.172v14.758c0,1.746,1.417,3.177,3.172,3.177h20.27 c1.747,0,3.176-1.431,3.176-3.177v-14.758c0-1.755-1.429-3.172-3.176-3.172h-0.707v2.293c0,1.272-1.039,2.31-2.311,2.31h-1.606 c-1.272,0-2.302-1.038-2.302-2.31v-2.293h-6.032v2.293c0,1.272-1.029,2.31-2.302,2.31h-1.606c-1.272,0-2.302-1.038-2.302-2.31 v-2.293H100.285z M99.94,439.728h20.961v9.907c0,1.188-0.963,2.151-2.151,2.151h-16.658c-1.189,0-2.151-0.963-2.151-2.151V439.728z M101.839,440.977v1.659h-1.005v0.911h1.005v1.708h-1.005v0.911h1.005v1.705h-1.005v0.91h1.005v1.461h0.911v-1.461h3.25v1.461 h0.911v-1.461h3.25v1.461h0.911v-1.461h3.25v1.461h0.91v-1.461h3.254v1.461h0.911v-1.461h1.001v-0.91h-1.001v-1.705h1.001v-0.911 h-1.001v-1.708h1.001v-0.911h-1.001v-1.659h-0.911v1.659h-3.254v-1.659h-0.91v1.659h-3.25v-1.659h-0.911v1.659h-3.25v-1.659h-0.911 v1.659h-3.25v-1.659H101.839L101.839,440.977z M102.75,443.547h3.25v1.708h-3.25V443.547z M106.906,443.547h3.253v1.708h-3.253 V443.547z M111.067,443.547h3.25v1.708h-3.25V443.547z M115.227,443.547h3.254v1.708h-3.254V443.547z M102.75,446.166h3.25v1.705 h-3.25V446.166z M106.906,446.166h3.253v1.705h-3.253V446.166z M111.067,446.166h3.25v1.705h-3.25V446.166z M115.227,446.166h3.254 v1.705h-3.254V446.166z"></path>
										</g>
									</svg>
									<span>10 Days ago</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="latestPorpWrapper">
						<div class="latestPorpImage">
							<figure style="background-image: url(images/latestPorp1.jpg);">
								<span>
									<span>$556.885</span>
									<span>$3.500/sq ft</span>
								</span>
							</figure>
						</div>
						<div class="latestPorpCon">
							<div class="latestPorpConWrapper">
								<h4>Guaranteed Modern Home</h4>
								<span>
									<svg class="pin" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 188.171 305.961">
										<g>
											<g>
												<path d="M188.024,93.942L188.024,93.942C188.024,42.067,145.957,0,94.082,0S0.15,42.067,0.15,93.942 c0,0-8.055,99.292,93.932,212.019C195.887,193.438,188.045,94.307,188.024,93.942z M94.082,160.326 c-35.279,0-63.881-28.612-63.881-63.871c0-35.299,28.602-63.871,63.881-63.871c35.279,0,63.871,28.572,63.871,63.871 C157.953,131.714,129.361,160.326,94.082,160.326z"></path>
												<path d="M94.082,49.038c24.965,0,45.188,20.223,45.188,45.168c0,24.944-20.223,45.188-45.188,45.188 c-24.955,0-45.178-20.243-45.178-45.188C48.904,69.261,69.127,49.038,94.082,49.038z"></path>
											</g>
										</g>
									</svg>
									<small>Louis, Missouri, us</small>
								</span>
								<p>Bedroom: 4 Bathroom:2 Sq Ft:750 <br>Apartment</p>
							</div>
							<div class="latestPorpFooter">
								<div class="owner">
									<svg class="owner" xmlns="http://www.w3.org/2000/svg" viewBox="5.828 1.364 19.344 22.072">
										<g transform="translate(0,-952.36218)">
											<path d="M18.738,953.753c-0.402,0.202-1.44,0.347-2.363,0.411c-0.638,0.048-1.274,0.06-1.875,0.078 c-1.521-0.002-2.528,0.607-3.099,1.596c-0.545,0.944-0.72,2.204-0.736,3.619c-0.659-0.134-0.729,0.514-0.729,1.007 c0.108,0.702,0.589,1.637,1.178,1.364c0.253,0.835,0.597,1.702,1.022,2.495c0.336,0.626,0.72,1.199,1.155,1.666 c-0.168,0.452-0.314,1.035-0.457,1.534c-1.806,0.938-3.684,1.248-5.408,2.363c-0.499,0.34-1.077,1.093-1.093,2.154 c-0.178,1.141-0.338,2.317-0.503,3.472c-0.022,0.142,0.104,0.288,0.248,0.286h18.843c0.144,0.003,0.27-0.145,0.248-0.286 c-0.175-1.153-0.316-2.333-0.504-3.472c-0.014-1.054-0.518-1.867-1.123-2.17c-0.484-0.242-3.211-1.552-5.385-2.363l-0.396-1.387 c0.502-0.483,0.922-1.103,1.264-1.774c0.398-0.784,0.705-1.648,0.922-2.479c0.688,0.021,1.033-0.861,1.146-1.403 c0.012-0.602-0.111-1.109-0.759-0.992c-0.017-1.704-0.07-2.839-0.248-3.681c-0.185-0.877-0.522-1.452-1.062-1.991 C18.896,953.676,18.854,953.744,18.738,953.753z M18.746,954.249c0.432,0.455,0.693,0.898,0.853,1.65 c0.135,0.639,0.197,1.521,0.225,2.759c-0.266-0.816-0.884-1.588-1.619-1.875c-0.547-0.205-1.067-0.237-1.434-0.054 c-0.774,0.222-1.801,0.196-2.51,0c-0.484-0.19-0.997-0.107-1.433,0.054c-0.928,0.458-1.396,1.129-1.635,1.929 c0.066-1.056,0.252-1.963,0.635-2.626c0.496-0.859,1.287-1.356,2.696-1.349c0.637-0.014,1.323-0.039,1.882-0.077 C17.262,954.6,18.143,954.486,18.746,954.249z M14.043,957.17c0.914,0.246,1.953,0.276,2.945,0c0.129-0.065,0.6-0.089,1.045,0.077 c0.791,0.407,1.146,0.983,1.333,1.627c0.511,1.818,0.067,3.546-0.782,5.246c-0.732,1.438-1.756,2.502-3.115,2.502 c-1.436-0.153-2.312-1.461-2.897-2.542c-0.932-1.778-1.457-3.236-0.907-5.207c0.194-0.714,0.706-1.383,1.333-1.627 C13.366,957.12,13.671,957.099,14.043,957.17z M13.687,966.368c0.528,0.453,1.129,0.751,1.782,0.751 c0.71,0,1.333-0.242,1.875-0.643l0.264,0.922c-0.75,1.049-1.475,2.222-2.107,3.2c-0.76-1.008-1.293-2.253-2.107-3.2L13.687,966.368 z M13.176,967.926l1.929,2.96c-0.242,0.307-0.394,0.719-0.573,1.076c-0.034,0.229,0.057,0.29,0.209,0.442l-0.31,1.696l-2.363-5.71 C12.455,968.23,12.842,968.071,13.176,967.926z M17.832,967.926c0.356,0.13,0.734,0.28,1.115,0.434l-2.378,5.741l-0.31-1.696 c0.18-0.128,0.202-0.276,0.209-0.442c-0.181-0.352-0.354-0.743-0.558-1.068C16.586,969.859,17.119,968.97,17.832,967.926z M19.412,968.546c0.451,0.187,0.891,0.377,1.318,0.565l-1.721,2.58c-0.062,0.094-0.048,0.229,0.031,0.31l0.828,0.83l-2.254,2.471 h-1L19.412,968.546z M11.603,968.584l2.781,6.718h-1l-2.254-2.471l0.829-0.83c0.079-0.08,0.093-0.215,0.031-0.31l-1.712-2.565 C10.716,968.946,11.166,968.763,11.603,968.584L11.603,968.584z M9.813,969.32l1.65,2.472l-0.852,0.853 c-0.088,0.089-0.092,0.248-0.008,0.341l2.107,2.316H6.365l0.457-3.191c0.055-0.665,0.349-1.434,0.883-1.813 C7.988,970.108,8.836,969.729,9.813,969.32L9.813,969.32z M21.188,969.32c1.055,0.473,1.902,0.874,2.138,0.991 c0.378,0.189,0.853,0.858,0.853,1.768c0.144,1.072,0.303,2.15,0.457,3.223h-6.346l2.107-2.316c0.084-0.093,0.08-0.252-0.008-0.341 l-0.852-0.853L21.188,969.32z M15.407,971.335h0.186l0.349,0.697c-0.136,0.087-0.159,0.213-0.186,0.334l0.465,2.572l-0.147,0.363 h-1.146l-0.147-0.363l0.465-2.572c0-0.172-0.071-0.224-0.186-0.334L15.407,971.335z"></path>
										</g>
									</svg>
									<span>Maria Barlow</span>
								</div>
								<div class="postTime">
									<svg class="calender" xmlns="http://www.w3.org/2000/svg" width="26.618px" height="24.8px" viewBox="2.191 0 26.618 24.8" enable-background="new 2.191 0 26.618 24.8" xml:space="preserve">
										<g transform="translate(-94.922001,-429.7886)">
											<path d="M103.685,429.789c-0.771,0-1.395,0.623-1.395,1.391v4.6c0,0.768,0.626,1.391,1.395,1.391h1.61 c0.771,0,1.395-0.623,1.395-1.391v-4.6c0-0.77-0.625-1.391-1.395-1.391H103.685z M115.926,429.789c-0.77,0-1.391,0.623-1.391,1.391 v4.6c0,0.768,0.623,1.391,1.391,1.391h1.611c0.771,0,1.395-0.623,1.395-1.391v-4.6c0-0.77-0.627-1.391-1.395-1.391H115.926z M100.285,433.481c-1.755,0-3.172,1.419-3.172,3.172v14.758c0,1.746,1.417,3.177,3.172,3.177h20.27 c1.747,0,3.176-1.431,3.176-3.177v-14.758c0-1.755-1.429-3.172-3.176-3.172h-0.707v2.293c0,1.272-1.039,2.31-2.311,2.31h-1.606 c-1.272,0-2.302-1.038-2.302-2.31v-2.293h-6.032v2.293c0,1.272-1.029,2.31-2.302,2.31h-1.606c-1.272,0-2.302-1.038-2.302-2.31 v-2.293H100.285z M99.94,439.728h20.961v9.907c0,1.188-0.963,2.151-2.151,2.151h-16.658c-1.189,0-2.151-0.963-2.151-2.151V439.728z M101.839,440.977v1.659h-1.005v0.911h1.005v1.708h-1.005v0.911h1.005v1.705h-1.005v0.91h1.005v1.461h0.911v-1.461h3.25v1.461 h0.911v-1.461h3.25v1.461h0.911v-1.461h3.25v1.461h0.91v-1.461h3.254v1.461h0.911v-1.461h1.001v-0.91h-1.001v-1.705h1.001v-0.911 h-1.001v-1.708h1.001v-0.911h-1.001v-1.659h-0.911v1.659h-3.254v-1.659h-0.91v1.659h-3.25v-1.659h-0.911v1.659h-3.25v-1.659h-0.911 v1.659h-3.25v-1.659H101.839L101.839,440.977z M102.75,443.547h3.25v1.708h-3.25V443.547z M106.906,443.547h3.253v1.708h-3.253 V443.547z M111.067,443.547h3.25v1.708h-3.25V443.547z M115.227,443.547h3.254v1.708h-3.254V443.547z M102.75,446.166h3.25v1.705 h-3.25V446.166z M106.906,446.166h3.253v1.705h-3.253V446.166z M111.067,446.166h3.25v1.705h-3.25V446.166z M115.227,446.166h3.254 v1.705h-3.254V446.166z"></path>
										</g>
									</svg>
									<span>10 Days ago</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd testimonials" style="background-image: url(images/testimonialBg.jpg);">
		<div class="container">
			<div id="testimonialSlider" class="owl-carousel owl-theme">
				<div class="item">
					<div class="testimonialWrapper">
						<div class="quote"></div>
						<div class="testimonialCon">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed odio condimentum vestibulum sit amet et ex. Lorem ipsum dolor sit amet.</p>
							<div class="testimonialAvatar">
								<figure style="background-image: url(images/avatar.jpg);"></figure>
							</div>

							<div class="testimonialAuther">
								<h3>Marc A.</h3>
								<p>Business Partner, Food &amp; Beverage Industry</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="testimonialWrapper">
						<div class="quote"></div>
						<div class="testimonialCon">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed odio condimentum vestibulum sit amet et ex. Lorem ipsum dolor sit amet.</p>
							<div class="testimonialAvatar">
								<figure style="background-image: url(images/avatar.jpg);"></figure>
							</div>

							<div class="testimonialAuther">
								<h3>Marc A.</h3>
								<p>Business Partner, Food &amp; Beverage Industry</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="testimonialWrapper">
						<div class="quote"></div>
						<div class="testimonialCon">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed odio condimentum vestibulum sit amet et ex. Lorem ipsum dolor sit amet.</p>
							<div class="testimonialAvatar">
								<figure style="background-image: url(images/avatar.jpg);"></figure>
							</div>

							<div class="testimonialAuther">
								<h3>Marc A.</h3>
								<p>Business Partner, Food &amp; Beverage Industry</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php include 'include/footer.php'; ?>