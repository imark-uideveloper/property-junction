<?php include 'include/header.php'; ?>
<div class="pageContainer innerPage creers">
	<section class="banner innerBanner" style="background-image: url(images/innerBanner.jpg);">
		<div class="bannerContainer">
			<div class="bannerWrapper">
				<div class="container">
					<div class="bannerInner">
						<h1>Careers</h1>
						<p><a href="index">Home</a><span>/</span><span>Careers</span></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd">
		<div class="container">
			<h2>Careers</h2>
			<h3>Join our sales team</h3>
			<p>You are invited to join our dynamic sales team. We currently have positions available for experienced sales professionals. You need to be self-motivated with a positive attitude and drive in this ever changing market. Property Junction agents are team players, but you must also be able to work on your own initiative. You should have excellent knowledge and experience of the Dubai Property market, along with outstanding interpersonal skills, as our number one priority is the customer satisfaction. Send your CV and a brief covering letter outlining your real estate experience to: <a href="mailto:info@propertyjunction.ae">info@propertyjunction.ae</a></p>

			<h3>Join our rental team</h3>
			<p>We are looking for experienced leasing agents to join our rental department. Our rental division is a young, dynamic team that are looking for motivated, professional team players to join them. If you are a hard working and focused individual, and would like to join the team at Property Junction, then please send your CV with photo and a brief covering letter outlining your real estate experience to: <a href="mailto:info@propertyjunction.ae">info@propertyjunction.ae</a></p>

			<h3>Administration posts</h3>
			<p>We are always on the lookout for talented administrative staff. We firmly believe that our people ‘behind the scenes’ are the backbone of the company. If you are an excellent organizer, good with numbers and anything mathematical, artistic or creative, we want to hear from you. Please send your CV and a brief covering letter to: <a href="mailto:info@propertyjunction.ae">info@propertyjunction.ae</a></p>

			<h3>Management positions</h3>
			<p>If you have the experience and confidence to manage a department, whether it be Sales, Technical or Administration related, then we invite you to speak with us. Send your CV and covering letter to: <a href="mailto:info@propertyjunction.ae">info@propertyjunction.ae</a> </p>
		</div>
	</section>
</div>
<?php include 'include/footer.php'; ?>