<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
	<meta name="viewport" content="width=device-width, initial-scale=0">
	<title>Property Junction</title>

	<link rel="icon" type="image/png" href="images/favicon-32x32.png" sizes="32x32"/>
	<link rel="icon" type="image/png" href="images/favicon-16x16.png" sizes="16x16"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="css/style.css" />

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!-- jQuery -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<body>
	<header>
		<div class="container">
			<div class="headerContainer">
				<div class="logoContainer">
					<a href="index.php">
						<img src="images/logo.png" alt="Property Junction">
					</a>
				</div>
				<div class="navContainer">
					<div class="mainNav">
						<nav>
							<ul>
								<li class="menu-item current-menu-item"><a href="index.php" title="Home">Home</a></li>
								<li class="menu-item"><a href="properties.php" title="Properties">Properties</a></li>
								<li class="menu-item"><a href="aboutus.php" title="About Us">About Us</a></li>
								<li class="menu-item"><a href="careers.php" title="Careers">Careers</a></li>
								<li class="menu-item"><a href="services.php" title="Services">Services</a></li>
								<li class="menu-item"><a href="contactus.php" title="Contact Us">Contact Us</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>