		<footer>
			<div class="mainfooter">
				<div class="mainfooterContainer">
					<h2 class="title">Contact <span>Listing agents list client homes and negotiate the best possible price</span></h2>

					<div class="row">
						<div class="col-sm-6">
							<div class="rightFooter">
								<div class="adressInfo">
									<ul>
										<li>
											<div class="rowCell">
												<svg class="callIcon" xmlns="http://www.w3.org/2000/svg" viewBox="194.109 90.822 612.329 620.806">
													<g>
														<path d="M333.033,122.522l91.291,117.718c9.609,14.415,9.609,33.634-2.402,45.646l-62.463,62.462 c-26.426,28.829,168.169,223.423,196.997,196.998l62.463-62.463c12.012-12.012,31.231-12.012,45.646-2.402l120.12,91.291 c12.012,12.013,14.415,28.829,2.402,40.841L719.82,679.88l-4.806,7.207c-84.084,84.084-295.495-69.67-362.763-134.534 c-64.865-67.268-216.216-278.679-134.534-360.36l7.207-7.208l67.268-67.267C304.204,105.706,321.021,108.108,333.033,122.522z M532.433,209.009c-21.622-2.402-16.817-33.633,2.402-31.231c96.096,14.415,170.57,86.486,182.582,182.583 c4.806,19.219-28.828,24.024-31.23,2.402C674.174,283.483,611.712,221.021,532.433,209.009z M527.628,122.522 c-21.622-2.402-16.817-36.036,2.402-31.231c144.144,19.219,257.057,129.729,276.276,271.471 c2.402,21.622-28.829,26.426-31.231,4.805C755.855,240.24,654.955,139.339,527.628,122.522z"></path>
													</g>
												</svg>
											</div>
											<div class="rowCell">
												<a href="tel:+97142440640">+971 4 244 0640</a>
											</div>
										</li>
										<li>
											<div class="rowCell">
												<svg class="at" xmlns="http://www.w3.org/2000/svg" viewBox="116 4.64 768 790.72">
													<path d="M669.072,724.536c5.408,17.408-3.712,35.96-20.784,42.352c-56.256,21.072-109.224,28.473-177.84,28.473 C281.864,795.36,116,660.192,116,437.488C116,205.76,284.136,4.64,540.888,4.64C740.848,4.64,884,142.112,884,332.992 c0,165.864-93.168,270.352-215.856,270.352c-53.384,0-92.048-27.248-97.688-87.464h-2.272 c-35.256,57.96-86.359,87.464-146.575,87.464c-73.872,0-127.256-54.52-127.256-147.688c0-138.568,102.264-264.712,265.872-264.712 c27.24,0,56.544,3.712,82.16,9.68c30.48,7.088,50.16,36.52,45.152,67.4L661.352,429.52c-11.392,67.072-3.432,97.736,28.385,98.849 c48.855,1.16,110.176-61.336,110.176-191.992c0-147.688-95.416-262.456-271.512-262.456c-173.824,0-326.081,136.36-326.081,353.344 c0,189.721,121.6,297.672,290.848,297.672c45.04,0,92.112-7.567,132.2-22.119c8.656-3.145,18.224-2.601,26.472,1.495 c8.248,4.089,14.488,11.385,17.216,20.177L669.072,724.536z M577.256,285.28c-9.072-2.272-20.448-4.544-35.208-4.544 c-75.023,0-134.088,73.832-134.088,161.336c0,43.168,19.336,70.392,56.792,70.392c42.04,0,86.36-53.36,96.576-119.28L577.256,285.28 z"></path>
												</svg>
											</div>
											<div class="rowCell">
												<a href="mailto:info@propertyjunction.ae">info@propertyjunction.ae</a>
											</div>
										</li>
										<li>
											<div class="rowCell">
												<svg class="pin" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 188.171 305.961">
													<g>
														<g>
															<path d="M188.024,93.942L188.024,93.942C188.024,42.067,145.957,0,94.082,0S0.15,42.067,0.15,93.942 c0,0-8.055,99.292,93.932,212.019C195.887,193.438,188.045,94.307,188.024,93.942z M94.082,160.326 c-35.279,0-63.881-28.612-63.881-63.871c0-35.299,28.602-63.871,63.881-63.871c35.279,0,63.871,28.572,63.871,63.871 C157.953,131.714,129.361,160.326,94.082,160.326z"></path>
															<path d="M94.082,49.038c24.965,0,45.188,20.223,45.188,45.168c0,24.944-20.223,45.188-45.188,45.188 c-24.955,0-45.178-20.243-45.178-45.188C48.904,69.261,69.127,49.038,94.082,49.038z"></path>
														</g>
													</g>
												</svg>
											</div>
											<div class="rowCell">
												<p>
													Property Junction Real Estate <br>
													RERA ORN # 12338 909 Dusseldorf Business Point, <br>
													Al Barsha 1, P.O. Box 450228, Dubai, <br>
													United Arab Emirates
												</p>
											</div>
										</li>
									</ul>
								</div>
								<div class="socialLinks">
									<ul>
										<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
										<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
									</ul>
								</div>
								<div class="copyright">
									<p>© Copyright 2017 <a href="propertyjunction.ae">propertyjunction.ae</a> | All Rights Reserved.</p>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="leftFooter">
								<form class="footerForm siteForm">
									<div class="form-group">
										<input type="text" class="form-control firstCap" placeholder="First Name">
									</div>
									<div class="form-group">
										<input type="text" class="form-control firstCap" placeholder="Last Name">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Phone">
									</div>
									<div class="form-group">
										<input type="email" class="form-control" placeholder="Email">
									</div>
									<div class="form-group">
										<textarea class="form-control firstCap" placeholder="Message"></textarea>
									</div>
									<div class="form-group">
										<input type="submit" name="message" value="Submit">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="footerBgImg" style="background-image: url(images/footerImg.jpg);"></div>
			</div>
		</footer>

		<!-- Bootstrap JavaScript -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.0/bootstrap-slider.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js"></script>
		<script src="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js"></script>
		<script src="js/app.js"></script>
	</body>
</html>