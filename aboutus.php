<?php include 'include/header.php'; ?>
<div class="pageContainer innerPage aboutUs">
	<section class="banner innerBanner" style="background-image: url(images/innerBanner.jpg);">
		<div class="bannerContainer">
			<div class="bannerWrapper">
				<div class="container">
					<div class="bannerInner">
						<h1>About Us</h1>
						<p><a href="index">Home</a><span>/</span><span>About us</span></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd">
		<div class="container">
			<div class="aboutInnerContainer">
				<div class="aboutCon">
					<h2>About Us</h2>
					<p>Property Junction Real Estate (PJRE) is founded by a veteran Swiss private banker with an objective of providing bespoke real estate solutions to high net worth investors who are looking for an exclusive and dedicated investment advice on the real estate sector of Dubai.</p>
					<p>PJRE is a Dubai based, privately held, full service real estate brokerage firm which prides on employing RERA certified and trained property advisors, who stand ready to serve your property needs and excel in delivering the best customer service in the industry.</p>
					<p>From introduction to completion you will receive personal attention and support of an experienced and dedicated property advisor who will work closely with you to reach your goals. Whether buying, selling or leasing, property transaction is the most important decision an individual takes.</p>
				</div>
				<div class="aboutImg">
					<figure style="background-image: url(images/aboutImg.jpg);">
						<p>
							<span>MR. Amish Sheth</span>
							<span>Managing Director - Property Junction Real Estate</span>
							<span>RERA BRN # 288770</span>
						</p>
					</figure>
				</div>
			</div>

			<p></p>
			<p>As Dubai continues to grow, it has become extremely complicated for tenants and buyers to make appropriate choices that would meet their requirements. We at PJRE understand this and are equipped to offer tailored solutions for every individual’s need as no location is left untapped without our property advisors learning about the local market with the latest information from the most accurate and credible sources.</p>

			<p>Our strength is to offer bespoke solution that YOU need, to enable YOU to make the right decisions as investor or actual user of the property. Whether an individual or corporate investor, our unique targeted marketing and advertising techniques, our technological resources and database, our strategically located office and our total commitment to providing the best service, makes us the preferred choice in the industry.</p>

			<p>We at PJRE have a motto ".......your search stops here" as we want to be the ultimate choice for our clients. So if you are a Buyer, Seller, Owner or Tenant think no further as your search stops here</p>
		</div>
	</section>
</div>
<?php include 'include/footer.php'; ?>