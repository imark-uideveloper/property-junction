<?php include 'include/header.php'; ?>
<div class="pageContainer innerPage aboutUs">
	<section class="banner innerBanner" style="background-image: url(images/innerBanner.jpg);">
		<div class="bannerContainer">
			<div class="bannerWrapper">
				<div class="container">
					<div class="bannerInner">
						<h1>Thank You</h1>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd">
		<div class="container">
			<div class="thankyou-cover contact-page text-center">
				<div class="tick"><i class="fa fa-check" aria-hidden="true"></i></div>
				<div class="thnkyou-message"><p>Thank you for getting in touch. We will get back to you shortly.</p></div>
			</div>
		</div>
	</section>
</div>
<?php include 'include/footer.php'; ?>