<?php include 'include/header.php'; ?>
<div class="pageContainer innerPage contactUs">
	<section class="banner innerBanner" style="background-image: url(images/innerBanner.jpg);">
		<div class="bannerContainer">
			<div class="bannerWrapper">
				<div class="container">
					<div class="bannerInner">
						<h1>Contact Us</h1>
						<p><a href="index">Home</a><span>/</span><span>Contact us</span></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sectionPadd contactWrapper">
		<div class="container">
			<h2>Contact us</h2>
			<p>We'd love to hear from you. Complete the form below and we will be in touch!</p>
			<div class="contactContainer">
				<div class="contactInner">
					<form class="siteForm">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control firstCap" placeholder="First Name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control firstCap" placeholder="Last Name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Phone">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="email" class="form-control" placeholder="Email">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<textarea class="form-control firstCap" placeholder="Message"></textarea>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<input type="submit" name="message" value="submit">
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="contactInner">
					<h2>Get in Touch</h2>

					<div class="contactInfor">
						<ul>
							<li>
								<div class="rowCell">
									<i class="contactIcon pinicon"></i>
								</div>
								<div class="rowCell">
									<p>
										Property Junction Real Estate <br>
										RERA ORN # 12338 909 Dusseldorf Business Point, <br>
										Al Barsha 1, P.O. Box 450228, Dubai, <br>
										United Arab Emirates
									</p>
								</div>
							</li>
							<li>
								<div class="rowCell">
									<i class="contactIcon emailIcon"></i>
								</div>
								<div class="rowCell">
									<a href="mailto:info@propertyjunction.ae">info@propertyjunction.ae</a>
								</div>
							</li>
							<li>
								<div class="rowCell">
									<i class="contactIcon callIcon"></i>
								</div>
								<div class="rowCell">
									<a href="tel:+97142440640">+971 4 244 0640</a>
								</div>
							</li>
							<li>
								<div class="rowCell">
									<i class="contactIcon faxIcon"></i>
								</div>
								<div class="rowCell">
									<p>+971 4 244 0806</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div id="map" class="mapWrapper"></div>
</div>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCFpaIaYgSLqlwlhHp_4R-QksOKZV9UugQ&callback=initMap"></script>
<script>
	/************************************************ Google Map ************************************************/
	function initMap() {
		var lock = {lat: 25.1132428, lng: 55.2028571};
		var labels = 'Mangrove Yoga';
		var map = new google.maps.Map(document.getElementById('map'), {
			disableDefaultUI: true,
			center: lock,
			zoom: 14
		});

		// Create markers.
		var marker = new google.maps.Marker({
			icon: 'images/marker.png',
			map: map,
			position: lock
		});
	}
	/************************************************ Google Map ************************************************/
</script>
<?php include 'include/footer.php'; ?>